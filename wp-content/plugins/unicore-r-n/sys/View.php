<?php
namespace UnicoreRN\sys;

defined('UNICORERN_FILE') or die("No script access");
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 22.07.2016
 * Time: 9:19
 */
trait View
{
    private $templatePath;

    public function render($template, $vars = array())
    {
        if (!file_exists($this->templatePath . $template)) {
            throw new \Exception("Missing view file: {$this->templatePath}{$template}");
        }
        extract($vars);
        ob_start();
        require $this->templatePath . $template . '.php';
        ob_get_flush();
    }

    public function prepareTemplate($template, $vars = array())
    {
        if (!file_exists($this->templatePath . $template)) {

        }
        extract($vars);
        ob_start();
        require $this->templatePath . $template . '.php';
        return 123;
    }

    public function setTemplateFolder($templatePath)
    {
        $this->templatePath = $templatePath;
    }
}