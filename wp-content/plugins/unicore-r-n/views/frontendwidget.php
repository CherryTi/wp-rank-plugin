<?php
defined('UNICORERN_FILE') or die("No script access");
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 27.07.2016
 * Time: 11:31
 */
$numRows = ceil(count($posts) / $instance['perrow']);
$posts = array_chunk($posts, $instance['perrow']);
$width = $instance['postWidth'] == 0 ? (100 / $instance['perrow']) . '%' : $instance['postWidth'];

if (!empty($posts)):?>
    <div class="recNews"><i class="fa fa-gratipay icon"></i><h5>Популярные списки</h5></div>
    <?php foreach ($posts as $row): ?>
        <!--    <pre>-->
        <!--        --><?php //print_r($row)?>
        <!--    </pre>-->
        <div class="rrow">
            <?php foreach ($row as $post): ?>
                    <div class="upost" onclick="document.location.href='/<?= $post->post_name ?>'" style="width: <?= $width ?>">
                        <div class="content-in">
                            <div class="uimg"><?= get_the_post_thumbnail($post); ?></div>
                            <div class="utitle"><a href="/<?= $post->post_name ?>"><?= $post->post_title; ?></a></div>
                            <?php if (isset($instance['showDesc']) && $instance['showDesc'] == 1): ?>
                                <div class="udisk">
                                    <?php

                                    $descr = mb_substr(preg_replace('/\[.*\]/', '', strip_tags($post->post_content)), 0, 100);
                                    echo mb_strlen($descr) > 0 ? $descr . "..." : '';

                                    ?>
                                </div>
                            <?php endif; ?>
                            <div class="ucat">
                                <?php
                                $cat = get_the_category($post->ID);
                                $catname = mb_strtolower($cat[0]->name);
                                echo "<a href=\"/category/{$catname}\">{$cat[0]->name}</a>"
                                ?>
                            </div>
                        </div>
                    </div>
            <?php endforeach; ?>
        </div>
        <div class="clear"></div>
    <?php endforeach; ?>
<?php endif; ?>
<!--<pre>-->
<?php //print_r($posts)?>
<!--</pre>-->
