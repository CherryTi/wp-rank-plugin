<?php defined('UNICORERN_FILE') or die("No script access");?>

<p>
<label for="<?=$this->get_field_id('quantity');?>">Количество статей</label>
<input class="widefat" type="number" value="<?= (!empty($instance['quantity']))?$instance['quantity']:1?>" name="<?= $this->get_field_name('quantity')?>" min="1" id="<?=$this->get_field_id('quantity');?>"><br>
</p>
<p>
<label for="<?=$this->get_field_id('perrow');?>">Количество в ряду</label>
<input class="widefat" type="number" value="<?= (!empty($instance['perrow']))?$instance['perrow']:1?>" name="<?= $this->get_field_name('perrow')?>" min="1" id="<?=$this->get_field_id('perrow');?>"><br>
</p>

<p>
    <label for="<?=$this->get_field_id('showDesc');?>">Показывать описание&nbsp;</label>
    <input class="widefat" type="checkbox" <?= (($instance['showDesc']))?"checked":''?> value="1" name="<?= $this->get_field_name('showDesc')?>" id="<?=$this->get_field_id('showDesc');?>"><br>
</p>

<p>
    <label for="<?=$this->get_field_id('postWidth');?>">Ширина блока поста (0 - автоматически, пример: 100px, 20%)</label>
    <input class="widefat" type="text" value="<?= (!empty($instance['postWidth']))?$instance['postWidth']:0?>" name="<?= $this->get_field_name('postWidth')?>" min="1" id="<?=$this->get_field_id('postWidth');?>"><br>
</p>

<!--<label for="--><?//= $this->get_field_id('cat')?><!--">Категория статей</label>-->
<!--<select name="--><?//= $this->get_field_name('cat')?><!--" id="--><?//= $this->get_field_id('cat')?><!--">-->
<!--    <option --><?//= ($instance['cat']==0)? "selected":''; ?><!-- value="0">Все категории</option>-->
<!--    --><?php //if ($cats = get_categories()): ?>
<!--        --><?php //foreach ($cats as $cat): ?>
<!--            <option --><?//= ($instance['cat']==$cat->term_id)? "selected":''; ?><!-- value="--><?//=$cat->term_id?><!--">--><?//=$cat->name?><!--</option>-->
<!--        --><?// endforeach; ?>
<!--    --><?php //endif; ?>
<!--</select>-->

