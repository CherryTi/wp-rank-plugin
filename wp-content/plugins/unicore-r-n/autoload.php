<?php
defined('UNICORERN_FILE') or die("No script access");
spl_autoload_register(function($className){
    if(strtolower(substr($className, 0,strpos($className,'\\'))) == 'unicorern'){
        $path = substr($className, strpos($className,'\\'));
        if(file_exists(UNICORERN_ABSPATH."$path.php")){
            require UNICORERN_ABSPATH."$path.php";
        }
    }
});