<?php
require "ImageTypes.php";
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 21.07.2016
 * Time: 13:36
 */
class Image extends ImageTypes
{

    private $path;
    private $type;
    private $height;
    private $width;

    public function __construct($path)
    {
        $this->path = $path;
        $this->type = $this->imageType($this->path);
    }

    public function getType(){
        return $this->type;
    }

    function resizeImg($resizePath, $biggest = 100){

            # Setting defaults and meta

            list($this->width, $this->height) = $info = getimagesize($this->path);


            if($this->width >= $this->height){
                $ratio = $biggest/$this->width;
            } else {
                $ratio = $biggest/$this->height;
            }

            switch ($this->type) {
                case 'jpeg': $image = imagecreatefromjpeg($this->path);
                    break;
                case 'png': $image = imagecreatefrompng($this->path);
                    break;
                default:
                    throw new Exception("Invalid image type {$this->path}  {$this->type}");
            }

            $height_new = ($this->height * $ratio);
            $width_new = ($this->width * $ratio);

            $image_resized = imagecreatetruecolor($width_new, $height_new);

            if ($this->type=="png") {
                    imagealphablending($image_resized, false);
                    $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                    imagefill($image_resized, 0, 0, $color);
                    imagesavealpha($image_resized, true);
                }


            imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $width_new, $height_new, $this->width, $this->height);


            # Writing image according to type to the output destination and image quality
            switch ($this->type) {
                case 'jpeg': imagejpeg($image_resized, $resizePath);
                    break;
                case 'png': imagepng($image_resized,  $resizePath, 9);
                    break;
                default: return false;
            }

    }





}