<?php

/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 22.07.2016
 * Time: 11:01
 */
class Model
{
    public $table;
    public $db;

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    public function findAll($fields = array()){
        if(empty($fields)){
            $fields = "*";
        } else {
            $fields = implode(',',$fields);
        }

        return $this->db->get_results("SELECT $fields FROM $this->table");

    }

    public function findId($id){
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $query = "SELECT * FROM $this->table WHERE id='$id'";
        $res = $this->db->get_row($query,ARRAY_A);
        foreach ($res as $key => $value){
            $this->{$key}=$value;
        }
        return $this;

    }


    public function findWhere($cond = array()){
        if(empty($cond)){
            return $this->findAll();
        }

        $condition = '';

        foreach ($cond as $key => $value){
            $condition.=$key."='$value'";
        }
        $query = "SELECT * FROM $this->table WHERE $condition";

        return $this->db->get_results($query);
    }

}