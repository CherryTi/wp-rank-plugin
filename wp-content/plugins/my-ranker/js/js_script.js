jQuery('.btmVote').click(function (event) {
    var data = jQuery(this),
        id = data.attr('data-id'),
        like = this.getAttribute('data-like'),
        emphasise = this.getAttribute('data-emphasise');
    // confirm("id= "+id+" like="+like);

    if (data.hasClass("mark")) {
        return;
    }
    else if (emphasise == 'emphasise') {
        var data1 = data.closest('.vote').find('.float .mark');
        data1.removeClass('mark');
        var count1 = data1.next('span').text();
        count1--;
        data1.next('span').text(count1);

        data.addClass('mark');
        var count = data.next('span').text();
        count++;
        data.next('span').text(count);

    }
    else if (emphasise == '') {
        data.closest('li').addClass('emphasise');
        data.addClass('mark');
        var count = data.next('span').text();
        count++;
        data.next('span').text(count);

        var data1 = data.closest('.vote');
        data1.find('.float .like').attr('data-emphasise', 'emphasise');
        data1.find('.float .unlike').attr('data-emphasise', 'emphasise');
    }

    jQuery.ajax({
        url: my_object.ajax_url,
        type: 'POST',
        data: {
            action: 'my_action_ranker',
            id: id,
            like: like
        },
        success: function (msg) {
            console.log(msg);
            if (msg == -1)
                return;
            var p = jQuery.parseJSON(msg);

            jQuery('.like_' + id).text(p['like']);
            jQuery('.unlike_' + id).text(p['unlike']);
        }
    });
});

jQuery('.adduserbutton').click(function (e) {
    jQuery('.adduserform').toggle();

})
if ("FileReader" in window) {
    var uphoto = document.querySelector('#uphoto');
    if (uphoto !== null) {
        uphoto.addEventListener('change', function () {
            var imgBlock = document.querySelector('.uimg');
            while (imgBlock.firstChild) {
                imgBlock.firstChild.remove();
            }
            var file = this.files[0];

            var reader = new FileReader();

            reader.onload = function (e) {
                var img = document.createElement('img');
                img.src = e.target.result;
                imgBlock.appendChild(img);
            }
            reader.readAsDataURL(file);
        })
    }

}

var submitBtn = document.querySelector('#addusernewsubmit');
if(submitBtn!==null){
    submitBtn.addEventListener('click', function (e) {
        e.preventDefault();


        var form = new FormData(this.form);
        var elements = this.form.elements;
        var errors = [];

        if (elements.name.value.length == 0) {
            errors[errors.length] = 'Имя не должно быть пустым';
            elements.name.placeholder = 'Имя не должно быть пустым';
            elements.name.classList = 'errorMsg';

        }
        if (errors.length == 0) {
            var xhr = new XMLHttpRequest();

            xhr.open('POST', my_object.ajax_url, true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    console.log(xhr.response);
                    var response = JSON.parse(xhr.response);

                    if (response.code == 1) {
                        jQuery('.adduserform').slideUp();
                        elements.name.value = '';
                        elements.description.value = '';
                        var imgBlock = document.querySelector('.uimg');
                        while (imgBlock.firstChild) {
                            imgBlock.firstChild.remove();
                        }

                        var imgPlaceholder = document.createElement('DIV');
                        imgPlaceholder.classList = 'uimgplaceholder';
                        imgPlaceholder.innerHTML = '<span>Изображение</span>';
                        imgBlock.appendChild(imgPlaceholder);
                    }
                }
            }
            xhr.send(form);
        }
    })
}


var choseImgBtn = document.querySelector('#chooseImgBtn');

if(choseImgBtn!==null){
    choseImgBtn.addEventListener('click', function () {
        document.querySelector('#uphoto').click();
    })
}



