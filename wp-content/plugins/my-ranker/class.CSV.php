<?php

class CSV
{
	//принимает
	//массив значений
	//принимает название файла
	//и разделитель
	function put_CSV($mass, $file='file.csv', $sep=','){
		$fp=fopen($file, 'w');
		
		foreach($mass as $val){
			fputcsv($fp, $val, $sep);
		}
		fclose($fp);
	}

	//принимает
	//название файла
	//и разделитель
	//возвращает массив
	function get_CSV($file="file.csv", $sep=','){
		$f=fopen($file, 'rt') or die("Ошибка");	
		$mass=array();
		//открываем цикл для чтения данных
		for($i=0; $data=fgetcsv($f, 0, $sep); $i++){
			//берем количество полей в строке
			$num=count($data);
			$mass[]=$data;
		}
		fclose($f);
		return $mass;
	}

	// $fild='file';
	// $path=$_FILES[$fild]['name'];
	// if(copy_file_fild($fild, $path))
	// 	echo "good";
	// else
	// 	echo "not good";
	//функция просто копирует поле
	function copy_file_fild($fild='file', $path){
		//fild - название поля у формы
		//path - путь куда сохранять файл ( $_FILES[$file]['name'] )
		if($_FILES[$fild]['size']!='0'){
			if(copy($_FILES[$fild]['tmp_name'], $path)){
				return true;
			}
		}

		return false;
	}
}

?>