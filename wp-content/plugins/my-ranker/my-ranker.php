<?php
/*
Plugin Name: My Ranker
Description: My Ranker
*/
//error_reporting(E_ALL);
define("ABS_PATH", plugin_dir_path(__FILE__));
define("PLUGIN_URL", plugin_dir_url(__FILE__));


require_once('class.CSV.php');
require_once('class.ranker.php');
$path_to_php_file='my-ranker/my-ranker.php';
date_default_timezone_set('Europe/Minsk');
$obj=new ranker();
$obj->page_title='my-ranker';
$obj->menu_title='my-ranker';
$obj->short_description='short description';
$obj->add_to_page=1;
$obj->access_level=0;
$obj->num=20;//количество на странице
// $obj->my_action_export_catagory();
// wp_die();

add_action('activate_'.$path_to_php_file, array($obj, 'activate'));
add_action('deactivate_'.$path_to_php_file, array($obj, 'deactivate'));

add_action('admin_menu', array($obj, 'add_admin_page'));

add_action('wp_dashboard_setup', array($obj, 'addDashboardUserElements'));

//добавляем шорткод
add_shortcode('ranker', array($obj, 'show_ranker'));

//подключаем javascript файл на фронт
add_action('wp_enqueue_scripts', array($obj, 'include_scripts'));

//определяем обрабочик аякса для фронта
add_action('wp_ajax_my_action_ranker', array($obj, 'my_action'));//authorized 
add_action('wp_ajax_nopriv_my_action_ranker', array($obj, 'my_action'));//all

//подключаем javascript файл на back-end
add_action('admin_enqueue_scripts', array($obj, 'include_scripts_admin'));
//добавляем обработчик Ajax (import all)
// add_action('wp_ajax_my_action_import_all', array($obj, 'my_action_import_all'));
//обработчик Ajax для (import for category)
add_action("wp_ajax_my_action_import_catagory", array($obj, 'my_action_import_catagory'));
//обработчик Ajax для (export по категориям)
add_action("wp_ajax_my_action_export_catagory", array($obj, 'my_action_export_catagory'));


add_action("wp_ajax_user_add_brand", array($obj, 'add_user_brand'));
add_action("wp_ajax_nopriv_user_add_brand", array($obj, 'add_user_brand'));


