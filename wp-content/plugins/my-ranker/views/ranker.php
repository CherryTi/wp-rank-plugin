<ol class="ranker">
    <?php foreach ($result as $key => $val):
        if($val->approved==1):

        $emphasise = "";
        $like = "";
        $unlike = "";
        if (array_key_exists($val->id, $votes)) {
            $emphasise = "emphasise";

            if ($votes[$val->id] == 'ul') {
                $unlike = "mark";
            } else if ($votes[$val->id] == 'l') {
                $like = "mark";
            }
        } ?>
        <li class="clear <?= $emphasise ?>">
            <div class="discript">
                <div class="float center num relative"><span><?= ++$key ?></span></div>
                <div class="float vote">
                    <div class="vote-in">
                        <div class="float">
                            <span data-emphasise="<?= $emphasise ?>" data-id="<?= $val->id ?>" data-like="like"
                                  class="block btmVote font like <?= $like ?>"><i>+</i></span>
                            <span class="block counter like_<?= $val->id ?>"><?= $val->like1 ?></span>
                        </div>
                        <div class="float">
                            <span data-emphasise="<?= $emphasise ?>" data-id="<?= $val->id ?>" data-like="unlike"
                                  class="block btmVote font unlike <?= $unlike ?>"><i>-</i></span>
                            <span class="block counter unlike_<?= $val->id ?>"><?= $val->unlike1 ?></span>
                        </div>
                    </div>
                </div>

                <?php if ($result3[0]->show_img == '' && !empty($val->url_img)): ?>
                    <div class="float img"><img
                            src="<?php
                            if($val->userupload){
                                echo plugins_url('my-ranker/useruploaded/thumbs/'.$val->url_img);
                            } else {
                                echo plugins_url('my-ranker/categories/' . $identity . '/mini/' . $val->url_img);
                            }

                            ?>">
                    </div>
                <?php endif; ?>

                <div class="float name_d"><p><?= stripslashes($val->name) ?></p></div>
                <?php if (!empty($val->shop_link)) {
                    echo "<a target='_blank' class='byButton' href=\"$val->shop_link\">Купить</a>";
                }
                ?>
            </div>
            <?php if ($val->show_extended == true) {
                echo '<div class="discript-ext">';
                echo stripslashes_deep(apply_filters('the_content', $val->description));
                echo '</div>';
            } ?>
        </li>
    <?php
    endif;
    endforeach; ?>

</ol>
<form enctype="multipart/form-data" method="post" action="">
    <input type="hidden" name="action" value="user_add_brand">
    <input type="hidden" name="cat" value="<?=$identity?>">
    <div class="useraddnew">
        <div class="newnumber"><span><?= ++$key ?></span></div>
        <div class="newname"><input placeholder="Майкл Джексон" name="name" type="text" required></div>
        <div class="adduserbutton">+</div>
        <div class="addusertext"><span>Не хватает? Добавь!</span></div>
    </div>
    <div class="adduserform">
        <div class="uphoto">
            <div class="uimg">
                <div class="uimgplaceholder"><span>Изображение</span></div>
            </div>
            <div id="chooseImgBtn">Выбрать изображение</div>
            <input id="uphoto" type="file" name="image"></div>
        <div class="udescr"><label for="udescr">Краткое описание</label><textarea id="udescr" name="description"></textarea></div>
        <div class="usubmit"><input placeholder="Майкл Джексон" value="Добавить" id="addusernewsubmit" type="submit"></div>
    </div>
</form>
