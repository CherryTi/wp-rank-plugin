<table class='table_b'>
    <tr>
        <th>Название</th>
        <th>Описание</th>
        <th>Изображение</th>
        <th>Ссылка на магазин</th>
        <th>Время добавления</th>
        <th>Действие</th>
        <th>Загружено пользователем</th>
    </tr>
    <?php foreach ($result as $key => $val):
        $img = ""; ?>

        <?php if ($val->url_img != "" && $val->userupload==0) {
        $img = "<img width=\"100px\" src=\"" . plugins_url('my-ranker/categories/' . $val->id_cat . '/mini/' . $val->url_img) . "\">";
    } elseif($val->url_img != "" && $val->userupload==1){
        $img = "<img width='100px' src='".PLUGIN_URL.'useruploaded/thumbs/'.$val->url_img."'>";
    } ?>

        <tr class="<?php
            if ($val->userupload==1 && $val->approved==0){
                echo "needmoderate";
            } else {
                echo 'moderated';
            }
        ?>">
            <td><?=$val->name?></td>
            <td><a
                    href="?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=<?=$_GET['cat']?>&paged=<?=$_GET['paged']?>&edit=<?=$val->id?>\">Редактировать</a></td>
            <td><?=$img?></td>
            <td><a target="_blank" href="<?=$val->shop_link?>"><?=$val->shop_link?></a></td>
            <td><?php
                if(!empty($val->time)){
                echo date("H:i d-m-y", $val->time);
                }
                ?></td>
            <td>
                <div class='edit_delete'>

                    <a href="?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=<?=$_GET['cat']?>&paged=<?=$_GET['paged']?>&edit=<?=$val->id?>&useruploaded=<?=$val->userupload?>">Редактировать</a>
                    <a class='delete' href="?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=<?=$_GET['cat']?>&paged=<?=$_GET['paged']?>&url=<?=$val->url_img?>&delete=<?=$val->id?>\">Удалить</a>
                </div>
            </td>
            <td>
                <?= ($val->userupload)? "Да" : "Нет" ;?>
            </td>
        </tr>
    <?php
    endforeach; ?>
</table>