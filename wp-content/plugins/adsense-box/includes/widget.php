<?php
/*
 * Author: http://photoboxone.com/
 */
defined('ABSPATH') or die();

class Unicore_Adsense_Widget extends WP_Widget {
	

	
	public function __construct() {
		parent::__construct( 'Unicore_Adsense_Widget', 'Unicore Adsense', $widget_options = array(
			'classname'   => 'Unicore_Adsense_Widget',
			'description' => "Show an adsense inside of a widget."
		) );
		

	}

	public function widget( $args, $instance ) {
		echo $instance['code'];
	}
	
	function update( $new_instance, $old_instance) {
		return parent::update($new_instance, $old_instance);
	}

	function form( $instance ) {
		$name = $this->get_field_name('code');
		$id = $this->get_field_id('code');
		$code = $instance['code'];
		echo "<p><label for=\"$id\">Код баннера</label><textarea rows='10' type='text' class='widefat' name=\"$name\" id=\"$id\">$code</textarea></p>";
	}
	

	
}

// setup widget
add_action( 'widgets_init', function(){
	register_widget( 'Unicore_Adsense_Widget' );
});
