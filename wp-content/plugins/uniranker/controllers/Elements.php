<?php
namespace Uniranker\controllers;

use Uniranker\models\Element;
use Uniranker\models\Mlists;
use Uniranker\sys\Controller;
use Uniranker\sys\Request;
use Uniranker\sys\Validator;

/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 29.07.2016
 * Time: 20:14
 */
class Elements extends Controller
{
    public function index()
    {
        $this->render('elements/elementsindex');
    }

    public function add()
    {
        if (Request::isGet()) {
            $this->render('elements/elementsindex', array('active' => 'add'));
            $this->render('elements/addelement');
        }
        if(Request::isPost()){
            echo $description = Validator::sanitizeText(Request::post('description'));
            new Mlists();
        }
    }

    public function edit()
    {
        if (Request::isGet()) {
            $this->render('elements/elementsindex', array('active' => 'edit'));



            if(Request::get('id')===false){
                $this->render('elements/searchelementform');
            }

            
        }
    }

    public function export(){
        if(Request::isGet()){
            $this->render('elements/elementsindex', array('active'=>'export'));
        }
    }

}