<?php
namespace Uniranker\controllers;

use Uniranker\models\Categories;
use Uniranker\sys\Controller;
use Uniranker\sys\Request;

/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 29.07.2016
 * Time: 20:14
 */
class Lists extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $this->render('lists/listsindex');
    }

    public function add()
    {
        if (Request::isGet()) {
            $this->render('lists/listsindex');
            $this->render('lists/addlist');
        }
        if(Request::isPost()){
            $categories = new Categories();
            $save = $categories->assignValues(Request::post())->save();
            if($save){
                echo json_encode(array('code'=>1, 'data' => Request::post()));
            } else {
                $categories->showError();
            }
            wp_die();
        }

    }
}