<?php

namespace Uniranker\controllers\dashboard;
use Uniranker\models\Brand;
use Uniranker\sys\Dashboard;

/**
 * Class TopLikes
 * @package Ranker\controllers\dashboard
 */
class TopLikes extends Dashboard
{
    /**
     *
     */
    public function renderContent()
    {
        $brands = new Brand();
        $topLikes = $brands->query(
            "SELECT * FROM $brands->table ORDER BY like1+unlike1 DESC LIMIT 10"
        );
        $this->render('toplikes', array('topLikes'=>$topLikes));
    }
}