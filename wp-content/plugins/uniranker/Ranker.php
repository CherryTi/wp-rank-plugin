<?php
namespace Uniranker;


use Uniranker\controllers\Elements;
use Uniranker\controllers\Lists;
use Uniranker\sys\Controller;
use Uniranker\models\Categories;
use Uniranker\sys\Request;

class Ranker extends Controller
{
    public $request;

    var $page_title;
    var $menu_title;
    var $short_description;
    var $add_to_page;
    var $access_level;
    var $admin_options_name = 'my_ranker';
    var $admin_options;
    var $table_name;    //название таблицы в БД
    var $table_name2;    //вторая таблица в БД
    var $table_name3;    //таблица с категориями в БД
    public $userCookie;
    public $zipUploads = 'uploads';
    public $pluginDir;

    public function __construct()
    {

        global $wpdb;
        $this->request = new Request();
        $this->admin_options = $this->get_option();    //получаем опции
        $this->table_name = $wpdb->prefix . 'my_table_ranker';    //название таблицы
        $this->table_name2 = $wpdb->prefix . 'my_table_ranker2';//вторая таблица
        $this->table_name3 = $wpdb->prefix . 'my_table_ranker3';//третья таблица
        $this->userCookie();
        $this->pluginDir = plugin_dir_path(__FILE__);
        $this->setTemplateFolder($this->pluginDir.'views/');
        // $this->bd_instal();
    }

    function get_option()
    {
        $admin_options = array(
            'jal_db_version' => ''//версия базы данных
        );
        $current_options = get_option($this->admin_options_name);
        if (!empty($current_options))
            foreach ($current_options as $key => $value)
                $admin_options[$key] = $value;

        update_option($this->admin_options_name, $admin_options);
        return $admin_options;
    }

    function activate()
    {
        // wp_die('activate');
        $this->bd_instal();
    }

    function deactivate()
    {
        // wp_die('deactivate');
    }

    function add_admin_page()
    {
        global $_registered_pages;
        add_menu_page( 'Ранкер', 'Ранкер', 'moderate_comments', 'my-ranker', function(){},'dashicons-thumbs-up');
        
        //lists main page
        $lists = new Lists();
        add_submenu_page('my-ranker','Списки','Списки','moderate_comments','categories', array($lists,"index"));
        add_submenu_page(null,'Добавить список','Добавить список','moderate_comments','addcategory', array($lists,"add"));


        add_submenu_page(null,'Редактировать категорию','Редактировать категорию','moderate_comments','editcategory', function(){});
        
        //elements
        add_submenu_page('my-ranker',"Элементы",'Элементы','moderate_comments','elements', array(new Elements(), 'index'));
        add_submenu_page(null,"Добавить элемент",'Добавить элемент','moderate_comments','addelement', array(new Elements(), 'add'));
        add_submenu_page(null,"Редактировать элемент",'Редактировать элемент','moderate_comments','editelement', array(new Elements(), 'edit'));
        add_submenu_page(null,"Экспортировать элементы",'Экспортировать элементы','moderate_comments','exportelements', array(new Elements(), 'export'));


        add_submenu_page('my-ranker','Экспортировать в категорию','Экспортировать категорию','moderate_comments','exportcategoty', function(){});
        add_submenu_page('my-ranker','Импортировать категорию','Импортировать в категорию','moderate_comments','importtocategory', function(){});
    }

    //функция добавляет категорию
    function add_category()
    {
        global $wpdb;

        if($this->request->isPost()){
            if (empty($_POST['name']) || empty($_POST['identity'])) {
                echo "<div class='updated'>Заполните все поля</div>";
            } else {
                $ins = array(
                    'name'     => Request::post('name'),
                    'identity' => Request::post('identity'),
                    'show_img' => Request::post('show_img')
                );
                if (false === $wpdb->insert($this->table_name3, $ins)) {
                    $wpdb->show_errors();
                    $wpdb->print_error();
                }
                $catDir = $this->pluginDir . "categories/" . $_POST['identity'];

                if (!mkdir($catDir.'/mini/', 0777, true)) {
                    echo "Невозможно Создать папку";
                    wp_die();
                };
            }
        }
//        $submit = "AddNew";
//
//print_r($_POST);
//            if (isset($_POST['edit'])) {
//            foreach ($_POST as $key => $val)
//                $_POST[$key] = trim($val);
//            $_POST['show_img'] = $_POST['show_img'] != null ? $_POST['show_img'] : '';
//            if (empty($_POST['name'])) {
//                echo "<div class='updated'>Заполните все поля</div>";
//            } else {
//                $ins = array('name' => $_POST['name']);
//                if (false === $wpdb->update(
//                        $this->table_name3,
//                        array(
//                            'name' => $_POST['name'],
//                            'identity' => $_POST['identity'],
//                            'show_img' => $_POST['show_img']
//                        ),
//                        array('id' => $_POST['id'])
//                    )
//                ) {
//                    $wpdb->show_errors();
//                    $wpdb->print_error();
//                }
//                $_POST = null;
//            }
//        }
//        if (isset($_GET['edit'])) {
//            $submit = "edit";
//            $query = "SELECT * FROM {$this->table_name3} WHERE id='{$_GET['edit']}'";
//            if (false === $result = $wpdb->get_results($query)) {
//                $wpdb->show_errors();
//                $wpdb->print_error();
//            }
//            foreach ($result[0] as $key => $val)
//                $_POST[$key] = $val;
//        }
//
//        if (isset($_GET['delete'])) {
//            //вытягиваем все изображения и удаляем, сначала изображения потом и голосования
//            $query = "SELECT * FROM `{$this->table_name}` WHERE id_cat='{$_GET['delete']}'";
//            // echo "query=$query<br>";
//            if (false === $result = $wpdb->get_results($query)) {
//                $wpdb->show_errors();
//                $wpdb->print_error();
//            } else {
//                // 1 удаляем все изображения из папки
//                foreach ($result as $key => $val) {
//
//                    if (trim($val->url_img) == "")
//                        continue;
//
//                    $this->delete_img($val->url_img, $val->id);
//                }
//
//                // 2 удаляем все бренды
//                if (false === $wpdb->delete(
//                        $this->table_name,
//                        array('id_cat' => $_GET['delete'])
//                    )
//                ) {
//                    $wpdb->show_errors();
//                    $wpdb->print_error();
//                } else {
//                    // 3 удаляем все голосования
//                    if (false === $wpdb->delete(
//                            $this->table_name2,
//                            array('id_cat' => $_GET['delete'])
//                        )
//                    ) {
//                        $wpdb->show_errors();
//                        $wpdb->print_error();
//                    } else {
//                        // 4 удаляем саму категорию
//                        if (false === $wpdb->delete(
//                                $this->table_name3,
//                                array('identity' => $_GET['delete'])
//                            )
//                        ) {
//                            $wpdb->show_errors();
//                            $wpdb->print_error();
//                        }
//                    }
//
//                }
//
//            }
//
//        }

        //get categories
        $categories = new Categories();
        $categories = $categories->findAll();

        $this->render('addcategory', array('categories'=>$categories));



        //вывод формы
//        $query = "SELECT * FROM {$this->table_name3}";
//        if (false === $result = $wpdb->get_results($query)) {
//            $wpdb->show_errors();
//            $wpdb->print_errro();
//        }
//
//        echo "<table class=\"table_c\">";
//        echo "<tr><th>identity</th><th>name</th><th>Action</th></tr>";
//        foreach ($result as $key => $val) {
//            echo "<tr><td>{$val->identity}</td><td>{$val->name}</td><td><div class='edit_delete'> <a href='?page=my-ranker%2Fclass.ranker.php&addCategory&edit={$val->id}'>edit</a><a class='delete_cat' href='?page=my-ranker%2Fclass.ranker.php&addCategory&delete={$val->identity}'>delete</a></div></td></tr>";
//        }
//
//        echo "</table>";
    }

    //
    function add_edit_brand1()
    {
        global $wpdb;

        $query = "SELECT * FROM {$this->table_name3}";
        if (false === $result = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }

        $this->render('editbrand', array('result'=>$result));

        if (isset($_GET['cat'])) {
            // echo "<p>cat={$_GET['cat']}</p>";
            $this->add_edit_brand();
        }
    }

    //добавление/редактирование бренда
    function add_edit_brand()
    {


        global $wpdb;
        $submit = 'AddNew';
        if (isset($_POST['AddNew'])) {
            //копируем файл
            $catdir = $this->pluginDir . "categories/" . $_POST['id_cat'];

            if(!file_exists($catdir."/mini/")){
                if (!mkdir($catdir."/mini/", 0777, true)) {
                    echo "Невозможно Создать папку";
                    wp_die();
                };
            }
            //upload image
            $imageFile = new Uploader($_FILES['file'],$this->pluginDir.'/categories/'.$_POST['id_cat']);
            $pathToUploadedImage = $imageFile->upload();

            //resize image
            $optimizedImage = new Image($pathToUploadedImage);
            $optimizedImage->resizeImg($this->pluginDir.'categories/'.$_POST['id_cat']."/mini/".$imageFile->newName);

            //добавляем в таблицу
            $ins = array(
                'name' => $_POST['name'],
                'description' => $_POST['description'],
                'url_img' => $imageFile->newName,
                'id_cat' => $_POST['id_cat'],
                'like1' => $_POST['like1'],
                'unlike1' => $_POST['unlike1']
            );
            if (!$wpdb->insert($this->table_name, $ins)) {
                $wpdb->show_errors();
                $wpdb->print_error();
            }
            $_POST = null;    //обнуляем массив
        }

        //обновляем при редактировании
        if (isset($_POST['Edit'])) {
            $catdir = $this->pluginDir . "categories/" . $_POST['id_cat'];

            if(!file_exists($catdir."/mini/")){
                if (!mkdir($catdir."/mini/", 0777, true)) {
                    echo "Невозможно Создать папку";
                    wp_die();
                };
            }
            // echo '<pre>';
            // print_r($_POST);
            // echo '</pre>';
            // wp_die('end');
            $file_name = $_POST['del_file'];
            if ($_FILES['file']['size'] != '0') {
                //1 удаляем старый файл
                //нужно удалить старый файл
                // echo "<p>need delete old file_name=$file_name</p>";
                if (file_exists(dirname(__FILE__) . '/logo/' . $_POST['del_file'])
                    && isset($_POST['del_file']
                    )
                ) {
                    //удаляем файл
                    // echo "{$_POST['del_file']} - {$_POST['id']}<br>";
                    $this->delete_img($_POST['del_file'], $_POST['id']);
                }

                //2 копируем файл
                $imageFile = new Uploader($_FILES['file'],$this->pluginDir.'/categories/'.$_POST['id_cat']);
                $pathToUploadedImage = $imageFile->upload();
                $optimizedImage = new Image($pathToUploadedImage);
                $optimizedImage->resizeImg($this->pluginDir.'categories/'.$_POST['id_cat']."/mini/".$imageFile->newName);
            }
            //update data
            $ins = array(
                'name' => $_POST['name'],
                'description' => $_POST['description'],
                'url_img' => $imageFile->newName,
                'id_cat' => $_POST['id_cat'],
                'like1' => $_POST['like1'],
                'unlike1' => $_POST['unlike1']
            );
            if (false === $wpdb->update(
                    $this->table_name,
                    $ins,
                    array('id' => $_POST['id'])
                )
            ) {
                $wpdb->show_errors();
                $wpdb->print_error();
            }
            $_POST = null;
        }

        if (isset($_GET['edit']) || isset($_GET['del_img'])) {
            if (isset($_GET['del_img'])) {
                //delete file
                $this->delete_img($_GET['url'], $_GET['del_img']);
                if (false === $wpdb->update(
                        $this->table_name,
                        array('url_img' => ''),
                        array('id' => $_GET['del_img'])
                    )
                ) {
                    $wpdb->show_errors();
                    $wpdb->print_error();
                }
                $_GET['edit'] = $_GET['del_img'];
            }
            $query = "SELECT * FROM {$this->table_name} WHERE id='{$_GET['edit']}'";
            if (false === $result = $wpdb->get_results($query)) {
                $wpdb->show_errors();
                $wpdb->print_error();
            }

            foreach ($result[0] as $key => $val)
                $_POST[$key] = $val;

            $submit = "Edit";
        }
        if (isset($_GET['delete'])) {
            // 1 удаляем само изображение
            $this->delete_img($_GET['url'], $_GET['delete']);
            // 2 удаляем из таблицы
            if (false === $wpdb->delete(
                    $this->table_name,
                    array('id' => $_GET['delete'])
                )
            ) {
                $wpdb->show_errors();
                $wpdb->print_error();
            } else {
                // 3 удаляем все голосования для бренда
                if (false === $wpdb->delete(
                        $this->table_name2,
                        array('id_brand' => $_GET['delete'])
                    )
                ) {
                    $wpdb->show_errors();
                    $wpdb->print_error();
                }
            }
        }

        // echo '<pre>';
        // print_r($this);
        // echo '</pre>';
        $path = admin_url("admin.php?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat={$_GET['cat']}&paged={$_GET['paged']}");
        $url_img = '';
        $del = '';
        $del_file = '';//удаляемый файл изображения при редактировании

        if ($_POST['url_img'] != '') {
            $url_img = "<img width=150 src=\"" . plugins_url('/categories/'. $_GET['cat']. "/mini/" . $_POST['url_img'], __FILE__) . "\">";
            $del = "<a class='delete' href=\"?page=my-ranker%2Fclass.ranker.php&addEditBrand&del_img={$_POST['id']}&url={$_POST['url_img']}&cat={$_GET['cat']}&paged={$_GET['paged']}\">delete-img</a>";
            $del_file = "<input type=\"hidden\" name=\"del_file\" value=\"{$_POST['url_img']}\">";
        }
        $id = '';
        if (isset($_POST['id']))
            $id = "<input type=\"hidden\" name=\"id\" value=\"{$_POST['id']}\">";
        //вывод всех блоков
        $query = "SELECT * FROM {$this->table_name3}";
        if (!false === $res = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }


        echo <<<EOF
<h2>{$this->page_title}</h2>
<p>{$this->menu_title}</p>

<table>
<tr>
<form enctype="multipart/form-data" action="{$path}" method="POST">
<td>name:</td><td> <input type="text" size="50" name="name" value="{$_POST['name']}"></td></tr>
<input type="hidden" name="description" value="{$_POST['description']}">
<tr><td>{$url_img} {$del_file} {$id}</td><td>{$del}</td></tr>
<tr><td></td><td><input type="file" name="file" accept="image/*,image/jpeg"></td></tr>
<tr><td>категория</td><td>
EOF;
        echo "<select name='id_cat'>";
        foreach ($res as $key => $val) {
            $selected = $_GET['cat'] === $val->identity ? 'selected' : '';
            echo "<option $selected value='{$val->identity}'>{$val->name}</option>";
        }
        echo "</select></td></tr>";

        $like1 = isset($_POST['like1']) ? $_POST['like1'] : 0;
        $unlike1 = isset($_POST['unlike1']) ? $_POST['unlike1'] : 0;

        echo <<<EOF
<tr><td>like<br><input type="text" size="4" name="like1" value="{$like1}"></td><td>unlike<br><input type="text" size="4" name="unlike1" value="{$unlike1}"></td></tr>
<tr><td><input type="submit" name="{$submit}" value="{$submit}"></td><td></td></tr>
</form>
</table>
EOF;


        // Переменная хранит число сообщений выводимых на станице
        $num = $this->num;
        // Извлекаем из URL текущую страницу
        $page = $_GET['paged'];
        // Определяем общее число сообщений в базе данных
        $result = $wpdb->get_results("SELECT COUNT(*) FROM {$this->table_name} WHERE id_cat='{$_GET['cat']}'");
        $posts = current($result[0]);
        // Находим общее число страниц
        $total = intval(($posts - 1) / $num) + 1;
        // Определяем начало сообщений для текущей страницы
        $page = intval($page);
        // Если значение $page меньше единицы или отрицательно
        // переходим на первую страницу
        // А если слишком большое, то переходим на последнюю
        if (empty($page) or $page < 0) $page = 1;
        if ($page > $total) $page = $total;
        // Вычисляем начиная к какого номера
        // следует выводить сообщения
        $start = $page * $num - $num;
        // Выбираем $num сообщений начиная с номера $start

        //изымаем данные из таблицы
        //просто select запрос

        $query = "SELECT * FROM `{$this->table_name}` WHERE id_cat='{$_GET['cat']}' ORDER BY id LIMIT $start, $num";
        $result = $wpdb->get_results($query);

        echo "<hr><div class='paginate'>";

        for ($i = 1; $i <= $total; $i++) {
            if ($i != $page)
                //?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=2
                echo " <a href=\"?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat={$_GET['cat']}&paged=$i\">$i</a> ";
            else
                echo " <a class='mark_p' href=\"?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat={$_GET['cat']}&paged=$i\">$i</a> ";
        }
        echo "</div><hr>";

        $this->render('brandstable', array('result' => $result));


        echo "<hr><div class='paginate'>";

        for ($i = 1; $i <= $total; $i++) {
            if ($i != $page)
                //?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=2
                echo " <a href=\"?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat={$_GET['cat']}&paged=$i\">$i</a> ";
            else
                echo " <a class='mark_p' href=\"?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat={$_GET['cat']}&paged=$i\">$i</a> ";
        }
        echo "</div><hr>";

    }

    //функция импорта экспорта в таблицу
    function importExpor()
    {
        global $wpdb;

        if (isset($_POST['submit2'])) {
            echo '<pre>';
            print_r($_POST);
            echo '</pre>';
            //	wp_die('end');
        }

        $path = admin_url("admin.php?page=my-ranker%2Fclass.ranker.php&importExport");
// 		echo <<<EOF
// <h2>import/export</h2>
// <form id="form1" action="{$path}" method="POST">
// <input id="submit" type="submit" name="submit" value="submit">
// </form>
// <br>
// <hr>
        echo <<<EOF
<hr>
<h2>export the category</h2>
<form id="form2" action="{$path}" method="POST">
EOF;
        $query = "SELECT * FROM {$this->table_name3}";
        if (false === $result = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }

        echo "<select id='select' name='identity'>";
        echo "<option value='-1'>Экспотр всех категорий</option>";
        foreach ($result as $key => $val) {
            echo "<option value='{$val->identity}'>{$val->name}</option>";
        }
        echo "</select><br>";

        echo <<<EOF
sep: <input type="text" size="3" name="sep" value=";"><br>
<input id="submit2" type="submit" name="submit2" value="submit2">
</form>
<br>
<hr>
<h2>Import database</h2>
<form name="form3" action="{$path}" method="POST">
EOF;
        $query = "SELECT * FROM {$this->table_name3}";
        if (false === $result = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }

        echo "<select id='select3' name='identity'>";
        echo "<option value='-1'>Выберите категорию</option>";
        foreach ($result as $key => $val) {
            echo "<option value='{$val->identity}'>{$val->name}</option>";
        }
        echo "</select><br>";
        echo <<<EOF
sep: <input type="text" size="3" name="sep" value=";"><br>
<input type="file" multiple="multiple" name="file"><br>
<input id="submit3" type="submit" name="submit3" value="submit3">
</form>
EOF;
    }

    //функция создает таблицу в бд
    function bd_instal()
    {
        global $wpdb;
        $jal_db_version = "2.9";    //текущая версия бд
        $table_name = $this->table_name;
        $table_name2 = $this->table_name2;
        $table_name3 = $this->table_name3;
        //CREATE TABLE `wordpress_div`.`test` ( `one` INT NOT NULL , `two` INT NOT NULL , `three` INT NOT NULL , `four` INT NOT NULL ) ENGINE = MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;

        $sql = "CREATE TABLE " . $table_name . " (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				time VARCHAR(55) NOT NULL,
				name tinytext NOT NULL,
				description text NOT NULL,
				url_img VARCHAR(200) NOT NULL,
				like1 mediumint(9) NOT NULL DEFAULT '0',
				unlike1 mediumint(9) NOT NULL DEFAULT '0',
				id_cat VARCHAR(20) NOT NULL,
				UNIQUE KEY id (id)
			) ENGINE = MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
        //создаем запрос для таблицы ip
        $sql2 = "CREATE TABLE " . $table_name2 . " (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				time VARCHAR(55) NOT NULL,
				id_brand mediumint(9) NOT NULL,
				id_cat VARCHAR(20) NOT NULL,
				ip VARCHAR(55) NOT NULL,
				status VARCHAR(10) NOT NULL,
				UNIQUE KEY id (id)
			) ENGINE = MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";
        //создаем запрос для таблицы с категориями
        $sql3 = "CREATE TABLE " . $table_name3 . " (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				identity VARCHAR(20) NOT NULL,
				name tinytext NOT NULL,
				show_img TINYTEXT NOT NULL,
				UNIQUE KEY id (id)
			) ENGINE = MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;";

        //если нету таблицы или нужно создать или обновить
        if ($wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name
            || $wpdb->get_var("SHOW TABLES LIKE '{$table_name2}'") != $table_name2
            || $wpdb->get_var("SHOW TABLES LIKE '{$table_name3}'") != $table_name3
            || $this->admin_options['jal_db_version'] != $jal_db_version
        ) {
            // echo "sql=$sql<br>";
            // echo "sql2=$sql2<br>";
            // echo "sql3=$sql3<br>";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);    //создаем обновляем первую таблицу
            dbDelta($sql2);    //создаем обновляем вторую таблицу
            dbDelta($sql3);    //создаем обновляем вторую таблицу

            //обновляем опции в таблице
            $this->admin_options['jal_db_version'] = $jal_db_version;
            update_option($this->admin_options_name, $this->admin_options);
        }
    }

    //добавление лайка
    function add_like($id)
    {
        global $wpdb;
        //проверяем было ли голосование
        $query = "SELECT * FROM {$this->table_name2} WHERE id_brand='{$id}' AND ip='{$this->userCookie}'";
        // echo "query=$query<br>";
        if (false === $result = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
            return -1;
        }

        //не сохранено в базе данных
        if (!$result
            || ($result && $result[0]->status == 'ul')
            || ($result && $result[0]->status == 'n')
        ) {
            // if(!$result){
            $query = "SELECT `like1`, `unlike1`, `id_cat` FROM {$this->table_name} WHERE id='{$id}'";
            // echo "query=$query<br>";
            if (false === $result1 = $wpdb->get_results($query)) {
                $wpdb->show_errors();
                $wpdb->print_error();
                return -1;
            }

            $status = '';
            if ($result[0]->status == 'ul') {        //was unlike
                // echo "ul<br>";
                $result1[0]->unlike1--;
                $result1[0]->like1++;// и увеличиваем like
                $status = 'l';
            }
            // else if($result[0]->status=='n'){	//was like
            // 	// echo "n<br>";
            // 	$result1[0]->like1++;
            // 	$status='l';
            // }
            else {    //first time, add new row in table
                // echo "not save in database<br> {$result1[0]->id_cat}";
                $result1[0]->like1++;
                $status = 'l';
                //добавляем в таблицу ip

                $ins = array(
                    'id_brand' => $id,
                    'ip' => $this->userCookie,
                    'status' => $status,
                    'time' => current_time('mysql'),
                    'id_cat' => $result1[0]->id_cat
                );
                // print_r($ins);

                if (false === $wpdb->insert(
                        $this->table_name2,
                        $ins
                    )
                ) {
                    $wpdb->show_errors();
                    $wpdb->print_error();
                    return -1;
                }
            }

            if (false === $wpdb->update(
                    $this->table_name,
                    array(
                        'like1' => $result1[0]->like1,
                        'unlike1' => $result1[0]->unlike1
                    ),
                    array('id' => $id)
                )
            ) {
                $wpdb->show_errors();
                $wpdb->print_error();
                return -1;
            }

            if (false === $wpdb->update(
                    $this->table_name2,
                    array(
                        'status' => $status
                    ),
                    array(
                        'id_brand' => $id,
                        'ip' => $this->userCookie
                    )
                )
            ) {
                $wpdb->show_errors();
                $wpdb->print_error();
                return -1;
            }
            //возвращаем результаты
            $res = array();
            $res['like'] = $result1[0]->like1;
            $res['unlike'] = $result1[0]->unlike1;
            return json_encode($res);
        }//не сохранено в базе данных
        else {
            return -1;
        }
    }

    function add_unlike($id)
    {
        global $wpdb;
        //проверяем было ли голосование
        $query = "SELECT * FROM {$this->table_name2} WHERE id_brand='{$id}' AND ip='{$this->userCookie}'";
        // echo "query=$query<br>";
        if (false === $result = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
            return -1;
        }

        //не сохранено в базе данных
        if (!$result
            || ($result && $result[0]->status == 'l')
            || ($result && $result[0]->status == 'n')
        ) {
            // if(!$result){
            $query = "SELECT `like1`, `unlike1`, `id_cat` FROM {$this->table_name} WHERE id='{$id}'";
            // echo "query=$query<br>";
            if (false === $result1 = $wpdb->get_results($query)) {
                $wpdb->show_errors();
                $wpdb->print_error();
                return -1;
            }
            // echo '<pre>';
            // print_r($result1);
            // echo '</pre>';
            // wp_die('end');

            $status = '';
            if ($result[0]->status == 'l') {        //was like
                // echo "l<br>";
                $result1[0]->like1--;
                $unlike = $result1[0]->unlike1++;//+1 unlike
                $status = 'ul';
            }
            // else if($result[0]->status=='n'){	//was unlike
            // 	// echo "n<br>";
            // 	$unlike=$result1[0]->unlike1++;
            // 	$status='ul';
            // }
            else {                            //first time
                // echo "not save  in database<br>'{$result1[0]->id_cat}'";
                $status = 'ul';
                $unlike = $result1[0]->unlike1++;

                $ins = array(
                    'id_brand' => $id,
                    'ip' => $this->userCookie,
                    'status' => $status,
                    'time' => current_time('mysql'),
                    'id_cat' => $result1[0]->id_cat
                );
                // echo '<pre>';
                // print_r($ins);
                // echo '</pre>';
                // wp_die('end');
                //добавляем в таблицу ip
                if (false === $wpdb->insert(
                        $this->table_name2,
                        $ins
                    )
                ) {
                    $wpdb->show_errors();
                    $wpdb->print_error();
                    return -1;
                }
            }

            if (false === $wpdb->update(    //обновляем первую таблицу
                    $this->table_name,
                    array(
                        'like1' => $result1[0]->like1,
                        'unlike1' => $result1[0]->unlike1
                    ),
                    array('id' => $id)
                )
            ) {
                $wpdb->show_errors();
                $wpdb->print_error();
                return -1;
            }
            if (false === $wpdb->update(    //обновляем вторую таблицу
                    $this->table_name2,
                    array(
                        'status' => $status
                    ),
                    array(
                        'id_brand' => $id,
                        'ip' => $this->userCookie
                    )
                )
            ) {
                $wpdb->show_errors();
                $wpdb->print_error();
                return -1;
            }

            //возвращаем результаты
            $res = array();
            $res['like'] = $result1[0]->like1;
            $res['unlike'] = $result1[0]->unlike1;
            return json_encode($res);
        } else {
            return -1;
        }
    }

    //функция показывает ranker по шорткоду
    function show_ranker($atts)
    {
        $atts = shortcode_atts(
            array(                //значение по умолчанию
                'identity' => '-1',
            ),
            $atts,                //массив из функции
            'ranker'            //шотркод
        );
        extract($atts);

        if ($id === -1)
            return;

        global $wpdb;

        $query = "SELECT * FROM {$this->table_name3} WHERE identity='{$identity}'";
        if (false === $result3 = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }

        $query = "SELECT * FROM {$this->table_name} WHERE {$this->table_name}.id_cat='{$identity}'   ORDER BY ({$this->table_name}.like1 - {$this->table_name}.unlike1) DESC";

        $userVotes = $wpdb->get_results("SELECT {$this->table_name2}.id_brand, {$this->table_name2}.status FROM {$this->table_name2} WHERE ip='{$this->userCookie}' AND {$this->table_name2}.id_cat='{$identity}'");
        $votes = array();
        foreach ($userVotes as $key => $value) {
            $votes[$value->id_brand] = $value->status;
        }


        if (false === $result = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }
        $res = $this->prepareTemplate('ranker', array(
            'result'=>$result,
            'votes'=>$votes,
            'identity'=> $identity,
            'result3' => $result3
        ));
        return $res;
    }

    //подключаем js and css files
    function include_scripts()
    {
        //проверка есть ли шорткод на странице
        // wp_die('end' );
        global $post;
        if (!has_shortcode($post->post_content, 'ranker')) {
            return; //ничего не подключаем
        }
        //добавляем файл в очередь
        wp_enqueue_script('js_script', plugins_url('/js/js_script.js', __FILE__), array('jquery'), null, true);

        wp_localize_script('js_script', 'my_object', array('ajax_url' => admin_url('admin-ajax.php')));

        //подключаем css
        wp_enqueue_style('my_style', plugins_url('/css/style.css', __FILE__));
    }

    //подключаем js и css на backend
    function include_scripts_admin()
    {
//        if ($_GET['page'] !== 'my-ranker') {
//            return; //ничего не делаем
//        }
        //подключаем css
        wp_enqueue_style('admin_style', plugins_url('/css/admin_style.css', __FILE__));

        wp_enqueue_script('admin_scripts', plugins_url('/js/admin_scripts.js', __FILE__), array('jquery'), null, true);
        //можно подключать остальное все
        //..... ... ....
        wp_localize_script(
            'admin_scripts',
            'my_object',
            array(
                'ajax_url' => admin_url('admin-ajax.php'),
                'path_to_file' => plugins_url('/', __FILE__)
            )
        );
    }

    //action ajax
    function my_action()
    {
        if (isset($_POST['cl']))
            $cl = $_POST['cl'];
        if (isset($_POST['id']))
            $id = (int)$_POST['id'];
        if (isset($_POST['like']))
            $flag = $_POST['like'];

        if ($_POST['like'] === 'like')
            echo $this->add_like($_POST['id']);
        else if ($_POST['like'] === 'unlike')
            echo $this->add_unlike($_POST['id']);

        //echo "id=$id flag=$flag";

        wp_die();// прерываем хук
    }

    //импорт по категориям
    function my_action_import_catagory()
    {
        global $wpdb;
        $_POST['sep'] = trim($_POST['sep']);
        $sep = empty($_POST['sep']) ? ',' : $_POST['sep'];

        // $_POST['identity']='id1';
        // $sep=',';

        if (isset($_POST['identity']))
            $identity = trim($_POST['identity']);
        if ($identity == -1) {
            $query = "SELECT * FROM {$this->table_name} ORDER BY id_cat, id";
        } else
            $query = "SELECT * FROM {$this->table_name} WHERE id_cat='{$_POST['identity']}'";


        $res = array();
        // $res[]='table1';
        $res[] = array(
            "name",
            "description",
            "url_img",
            "like1",
            "unlike1",
            "id_cat"
        );

        foreach ($result as $key => $val)
            $res[] = array(
                $val->name,
                $val->description,
                $val->url_img,
                $val->like1,
                $val->unlike1,
                $val->id_cat
            );

        $obj = new CSV();

        $path = plugin_dir_path(__FILE__) . 'file2.csv';
        // echo $path;
        $obj->put_CSV($res, $path, $sep);
        $path1 = plugins_url('file2.csv', __FILE__);
        echo $path1;

        wp_die();//прерывание
    }

    //экспорт по категориям
    function my_action_export_catagory()
    {



        if (!is_writable($this->pluginDir)) {
            echo "Директория $this->pluginDir должны быть доступной для записи";
        }
        global $wpdb;
        $sep = trim($_POST['sep']);

        if (isset($_POST['identity']))
            $identity = $_POST['identity'];
        if ($identity == -1) {
            echo "Не выбрана категория";
            wp_die();
        }

        $upload = new Uploader($_FILES['file'], $this->pluginDir.$this->zipUploads);
        $pathToFile = $upload->upload();

        $zip = new ZipArchive();
        if (!$zip->open($pathToFile)) {
            echo "Невозможно открыть файл";
            wp_die();
        }

        $catdir = $this->pluginDir . "categories/" . $identity;

        if(!file_exists($catdir."/mini/")){
            if (!mkdir($catdir."/mini/", 0777, true)) {
                echo "Невозможно Создать папку";
                wp_die();
            };
            echo 'Создал папку '.$catdir."/mini/";
        }

        $zip->extractTo($catdir);
        $dir = opendir($catdir);

        $file = null;
        while ($file = readdir($dir)) {
            if (preg_match('/.*\.csv/', $file)) {
                $csvFile = $file;
            }
            if (preg_match('/.*\.jpeg/', $file) || preg_match('/.*\.png/', $file)){
                $image = new Image($catdir.'/'.$file);
                $image->resizeImg($catdir.'/mini/'.$file);
            }


        }

        $obj=new CSV();

        $mass=$obj->get_CSV($catdir.'/'.$csvFile, $sep);
        $mass=array_slice($mass, 1);
        // foreach($mass as $key => $val){
        // 	print_r($val);
        // }

        //добавление данных
        foreach($mass as $key => $val){
            $ins=array(
                'name'=>iconv('windows-1251', 'utf-8', $val[0]),
                'description'=>$val[1],
                'url_img'=>$val[2],
                'like1'=>$val[3],
                'unlike1'=>$val[4],
                'id_cat'=>$identity
            );

            if(!$wpdb->insert($this->table_name, $ins)){
                $wpdb->show_errors();
                $wpdb->print_error();
            }
        }
        unlink($path);//удаляем файл

        echo "Успешно добавлены";//успешно добавлены

        wp_die();//завершение ajax hook
    }

    //принимает изображение и id и удаляет файл
    function delete_img($img, $id)
    {
        global $wpdb;
        //проверяем есть ли еще такой файл
        $query = "SELECT * FROM {$this->table_name} WHERE url_img='{$img}' AND id!='{$id}'";
        // echo "query=$query<br>";
        if (false === $res = $wpdb->get_results($query)) {
            $wpdb->show_errors();
            $wpdb->print_error();
        }
        if (!$res) {
            unlink(dirname(__FILE__) . '/logo/' . $img);
        }
    }

    public function userCookie()
    {
        if (isset($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
            $this->userCookie = $_COOKIE['user_id'];
            return true;
        }
        setcookie('user_id', md5(uniqid() . microtime(true) . $_SERVER['REMOTE_ADDR']), time() + 60 * 60 * 24 * 365, '/');

    }
}