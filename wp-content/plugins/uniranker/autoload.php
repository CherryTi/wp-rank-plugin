<?php

spl_autoload_register(function($className){
    if(strtolower(substr($className, 0,strpos($className,'\\'))) == 'uniranker'){
        $path = substr($className, strpos($className,'\\'));
        if(file_exists(UNIRANKER_ABSPATH."$path.php")){
            require UNIRANKER_ABSPATH."$path.php";
        }
    }
});