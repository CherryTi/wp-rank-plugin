<?php
/*
Plugin Name: UniRanker
Description: UniRanker
Plugin URI: https://unicore.by
*/
//error_reporting(E_ALL);
define("UNIRANKER_ABSPATH", plugin_dir_path(__FILE__));
define("UNIRANKER_FILE", __FILE__);
define("UNIRANKER_BASENAME",plugin_basename(UNIRANKER_FILE));
define("UNIRANKER_VIEWS",UNIRANKER_ABSPATH."views/");

require 'autoload.php';

use Uniranker\Ranker;
use Uniranker\controllers\dashboard\TopLikes;

$path_to_php_file='my-ranker/UniRanker.php';
$obj=new Ranker();
$obj->page_title='UniRanker';
$obj->menu_title='UniRanker';
$obj->short_description='short description';
$obj->add_to_page=1;
$obj->access_level=5;
$obj->num=20;//количество на странице
// $obj->my_action_export_catagory();
// wp_die();

add_action('activate_'.$path_to_php_file, array($obj, 'activate'));
add_action('deactivate_'.$path_to_php_file, array($obj, 'deactivate'));

add_action('admin_menu', array($obj, 'add_admin_page'));


//добавляем шорткод
add_shortcode('uranker', array($obj, 'show_ranker'));

//подключаем javascript файл на фронт
add_action('wp_enqueue_scripts', array($obj, 'include_scripts'));

add_action('wp_dashboard_setup', array(new TopLikes('ranker','Ранкер'), 'init'));
////определяем обрабочик аякса для фронта
//add_action('wp_ajax_my_action_ranker', array($obj, 'my_action'));//authorized
//add_action('wp_ajax_nopriv_my_action_ranker', array($obj, 'my_action'));//all

//подключаем javascript файл на back-end
add_action('admin_enqueue_scripts', array($obj, 'include_scripts_admin'));


add_action('wp_ajax_uranker_add_category', array(new \Uniranker\controllers\Lists(), 'add'));


////добавляем обработчик Ajax (import all)
//// add_action('wp_ajax_my_action_import_all', array($obj, 'my_action_import_all'));
////обработчик Ajax для (import for category)
//add_action("wp_ajax_my_action_import_catagory", array($obj, 'my_action_import_catagory'));
////обработчик Ajax для (export по категориям)
//add_action("wp_ajax_my_action_export_catagory", array($obj, 'my_action_export_catagory'));
//




