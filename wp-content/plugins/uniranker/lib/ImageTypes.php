<?php

namespace Uniranker\lib;

class ImageTypes
{
    private $types = array(
        '1'=>'gif',
        '2'=>'jpeg',
        '3'=>'png',
        '4'=>'swf',
        '5'=>'psd',
        '6'=>'bmp',
        '7'=>'tiff_ii',
        '8'=>'tiff_mm',
        '9'=>'jpc',
        '10'=>'jp2',
        '11'=>'jpx',
        '12'=>'jb2',
        '13'=>'swc',
        '14'=>'iff',
        '15'=>'wbmp',
        '16'=>'xbm',
        '17'=>'ico'
    );

    public function imageType($path){
        if($type = exif_imagetype($path)){
            return $this->types[$type];
        }
        return false;
    }

}