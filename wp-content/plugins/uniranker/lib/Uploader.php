<?php

/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 21.07.2016
 * Time: 11:53
 */
class Uploader
{

    public $name;
    public $newName;
    private $type;
    private $tmp_name;
    private $error;
    private $size;

    private $path;

    private $mimeTypes;

    public function __construct($file, $path, $mimeTypes = array())
    {
        $this->name = $file['name'];
        $this->type = $file['type'];
        $this->tmp_name = $file['tmp_name'];
        $this->error = $file['error'];
        $this->size = $file['size'];

        $this->path = rtrim(trim($path), "/");

        $this->mimeTypes = $mimeTypes;
    }


    public function upload(){
        $this->newName = microtime(true).$this->name;
        $path = $this->path."/".$this->newName;
        move_uploaded_file($this->tmp_name, $path);
        return $path;
    }
}