<div class="form">
    <form action="<?php \Uniranker\sys\Url::goAdminPage('addcategory') ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name='action' value="uranker_add_category">
        <div><label for="name">Название</label><input name="name" id="name" placeholder="Лучшие актеры" type="text">
        </div>
        <div><label for="alias">Идентификатор</label><input name="identity" id="alias" type="text"></div>
        <div><label for="showImage">Показывать изображения</label><input name="show_img" id="showImage" value="true" type="checkbox">
        </div>
        <div><input type="submit" id="submitForm" value="Добавить"></div>
    </form>
</div>

<script>
    document.getElementById('name').addEventListener('keyup', function(){
        document.getElementById('alias').value = translite(this.value);
    });


    function translite(str){
        var arr={'а':'a', 'б':'b', 'в':'v', 'г':'g', 'д':'d', 'е':'e', 'ж':'g', 'з':'z', 'и':'i', 'й':'y', 'к':'k', 'л':'l', 'м':'m', 'н':'n', 'о':'o', 'п':'p', 'р':'r', 'с':'s', 'т':'t', 'у':'u', 'ф':'f', 'ы':'i', 'э':'e', 'А':'A', 'Б':'B', 'В':'V', 'Г':'G', 'Д':'D', 'Е':'E', 'Ж':'G', 'З':'Z', 'И':'I', 'Й':'Y', 'К':'K', 'Л':'L', 'М':'M', 'Н':'N', 'О':'O', 'П':'P', 'Р':'R', 'С':'S', 'Т':'T', 'У':'U', 'Ф':'F', 'Ы':'I', 'Э':'E', 'ё':'yo', 'х':'h', 'ц':'ts', 'ч':'ch', 'ш':'sh', 'щ':'shch', 'ъ':'', 'ь':'', 'ю':'yu', 'я':'ya', 'Ё':'YO', 'Х':'H', 'Ц':'TS', 'Ч':'CH', 'Ш':'SH', 'Щ':'SHCH', 'Ъ':'', 'Ь':'',
            'Ю':'YU', 'Я':'YA', ' ':'_'};
        var replacer=function(a){return arr[a]||a};
        return str.replace(/([А-яёЁ]|\s)/g,replacer)
    }

    document.querySelector('#submitForm').addEventListener('click', function(e){
        e.preventDefault();
        var form = new FormData(this.form);
        var elements = this.form.elements;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/wp-admin/admin-ajax.php' ,true);

        xhr.onreadystatechange = function(){
            if(xhr.readyState==4){
                console.log(xhr.response);
                var response = JSON.parse(xhr.response);
                if(response.code==1){
                    elements.name.value = '';
                    elements.identity.value = '';
                    elements.show_img.checked = false;
                    var messageDiv = document.querySelector('#addcatmessage');

                    var mess = document.createElement('span');
                    mess.classList = 'addsuccmsg';
                    mess.innerHTML = 'Категория '+response.data.name+' успешно добавлена';
                    messageDiv.appendChild(mess);
                    setTimeout(function(){
                        messageDiv.remove();
                    }, 3000);

                }
            }
        }

        xhr.send(form);
    });


</script>