<?php
use Uniranker\sys\Url;
use Uniranker\sys\Request;
?>
<div class="menu">
    <ul>
        <li><a <?=(Request::get('page')=='addcategory')?"class='active'":"";?> href="<?= Url::goAdminPage('addcategory')?>">Создать список</a></li>
        <li><a href="<?= Url::goAdminPage('editcategory')?>">Редактировать список</a></li>
    </ul>
</div>
<div id="addcatmessage"></div>