<div class="menu">
    <ul>
        <li><a <?=(isset($active) && $active=='add')?"class='active'":'';?> href="<?=\Uniranker\sys\Url::goAdminPage('addelement')?>">Создать элемент</a></li>
        <li><a <?=(isset($active) && $active=='edit')?"class='active'":'';?> href="<?=\Uniranker\sys\Url::goAdminPage('editelement')?>">Редактировать элемент</a></li>
        <li><a <?=(isset($active) && $active=='export')?"class='active'":'';?> href="<?=\Uniranker\sys\Url::goAdminPage('exportelements')?>">Экспортировать элементы</a></li>
    </ul>
</div>