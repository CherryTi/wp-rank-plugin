<div class="form">
    <form action="<?php \Uniranker\sys\Url::goAdminPage('addelement')?>" method="post" enctype="multipart/form-data">
        <div><label for="name">Название</label><input name="name" id="name" placeholder="Nike" type="text"></div>
        <div><label for="type">Тип</label><select name="type" id="type">
                <option value="film">Фильм</option>
            </select></div>
        <div><label for="link">Ссылка на сайт</label><input name="link" type="text" id="link"></div>
        <div><label for="file">Изображение</label><input name="image" type="file" id="file"></div>
        <div><label for="description">Описание</label><?php wp_editor('', 'description') ?></div>
        <div><input type="submit" value="Добавить"></div>
    </form>
</div>