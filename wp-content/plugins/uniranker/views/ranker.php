<ol class="ranker">
    <?php foreach ($result as $key => $val):

        $emphasise = "";
        $like = "";
        $unlike = "";
        if (array_key_exists($val->id, $votes)) {
            $emphasise = "emphasise";

            if ($votes[$val->id] == 'ul') {
                $unlike = "mark";
            } else if ($votes[$val->id] == 'l') {
                $like = "mark";
            }
        } ?>
        <li class="clear <?= $emphasise ?>">
            <div class="float center num relative"><span><?= $key++ ?></span></div>
            <div class="float vote">
                <div class="float">
                    <span data-emphasise="<?= $emphasise ?>" data-id="<?= $val->id ?>" data-like="like"
                          class="block btmVote font like <?= $like ?>"><i>+</i></span>
                    <span class="block counter like_<?= $val->id ?>"><?= $val->like1 ?></span>
                </div>
                <div class="float">
                    <span data-emphasise="<?= $emphasise ?>" data-id="<?= $val->id ?>" data-like="unlike"
                          class="block btmVote font unlike <?= $unlike ?>"><i>-</i></span>
                    <span class="block counter unlike_<?= $val->id ?>"><?= $val->unlike1 ?></span>
                </div>
            </div>

            <?php if ($result3[0]->show_img == ''): ?>
                <div class="float img"><img src="<?= plugins_url('my-ranker/categories/' . $identity . '/mini/' . $val->url_img) ?>"></div>
            <?php endif; ?>

            <div class="float name_d"><p><?= $val->name ?></p></div>
        </li>
    <?php endforeach; ?>

</ol>

<ol class="ranker">