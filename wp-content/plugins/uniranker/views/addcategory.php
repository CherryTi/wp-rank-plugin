<h2>Добавление списка</h2>
<div>
<form action="" method="POST">
    <input type="hidden" name="id">
    <div>
        <label for="identity">Идентификатор:</label>
        <input id="identity" type="text" placeholder="BestModernActors" name="identity" value="">
    </div>
    <div>
        <label for="name">Название:</label>
        <input id="name" type="text" name="name" placeholder="Лучшие актёры современности" value="">
    </div>
    <div>
        <label for="show_img">Список без изображений:</label>
        <input id="show_img" type="checkbox" name="show_img">
    </div>
    <div>
        <input type="submit" name="addCategory" value="Добавить">
    </div>
</form>
    </div>
<div>
    <div class="clear"></div>
<?php if (!empty($categories)): ?>
    <table class="categoriesList">
        <tr>
            <th>Название</th>
            <th>Идентификатор</th>
            <th>Изображения</th>
            <th>Действия</th>
        </tr>
        <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= $category->name ?></td>
                <td><?= $category->identity ?></td>
                <td><?php if ($category->show_img == 'on') {
                        echo "Показывать";
                    } else {
                        echo "Не показывать";
                    } ?></td>
                <td>Удалить Редактировать</td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
</div>