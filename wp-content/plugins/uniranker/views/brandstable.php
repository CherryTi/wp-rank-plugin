<table class='table_b'>
    <tr>
        <th>Название</th>
        <th>Описание</th>
        <th>Изображение</th>
        <th>Действие</th>
    </tr>
    <?php foreach ($result as $key => $val):
        $img = ""; ?>

        <?php if ($val->url_img != "") {
        $img = "<img width=\"100px\" src=\"" . plugins_url('my-ranker/categories/' . $val->id_cat . '/mini/' . $val->url_img) . "\">";
    } ?>

        <tr>
            <td><?=$val->name?></td>
            <td><?=$val->description?></td>
            <td><?=$img?></td>
            <td>
                <div class='edit_delete'><a
                        href="?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=<?=$_GET['cat']?>&paged=<?=$_GET['paged']?>&edit=<?=$val->id?>\">Редактировать</a><a
                        class='delete'
                        href="?page=my-ranker%2Fclass.ranker.php&addEditBrand&cat=<?=$_GET['cat']?>&paged=<?=$_GET['paged']?>&url=<?=$val->url_img?>&delete=<?=$val->id?>\">Удалить</a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>