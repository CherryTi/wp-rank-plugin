<?php
namespace Uniranker\models;
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 22.07.2016
 * Time: 11:01
 */
class Model
{
    public $table;
    public $db;
    public $attributes;
    public $types;
    public $defaults;
    public $errors;

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
        $fields = $this->db->get_results("SHOW COLUMNS FROM $this->table");
        foreach ($fields as $field){
            $this->attributes[$field->Field]='';
            $this->types[$field->Field]=$field->Type;
            $this->defaults[$field->Field]=$field->Default;
        }
    }

    public function findAll($fields = array()){
        if(empty($fields)){
            $fields = "*";
        } else {
            $fields = implode(',',$fields);
        }

        return $this->db->get_results("SELECT $fields FROM $this->table");

    }

    public function findId($id){
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $query = "SELECT * FROM $this->table WHERE id='$id'";
        $res = $this->db->get_row($query,ARRAY_A);
        foreach ($res as $key => $value){
            $this->{$key}=$value;
        }
        return $this;

    }

    /**
     * Find in database by condition
     * @param array $cond
     * @return array|null|object
     */
    public function findWhere($cond = array()){
        if(empty($cond)){
            return $this->findAll();
        }

        $condition = '';

        foreach ($cond as $key => $value){
            $condition.=$key."='$value'";
        }
        $query = "SELECT * FROM $this->table WHERE $condition";

        return $this->db->get_results($query);
    }

    public function query($query){
        return $this->db->get_results($query);
    }

    /**
     * Save model in database
     * @return false|int
     */
    public function save(){
        return $this->db->insert($this->table, $this->attributes);
    }

    public function __set($name, $value){
        $this->attributes[$name]=$value;
    }

    /**
     * Assign array values to all model attributes, if value empty assigns default from database
     * @param array $values
     * @return $this
     */
    public function assignValues(array $values){
        foreach ($this->attributes as $key => &$attribute){
            $attribute = (isset($values[$key]))? $values[$key]:$this->defaults[$key];
        }
        return $this;
    }

    public function showError(){
        $this->db->show_errors();
        $this->db->print_error();
    }

}