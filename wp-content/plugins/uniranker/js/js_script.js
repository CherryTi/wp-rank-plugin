jQuery('.btmVote').click(function(event) {
	var data=jQuery(this),
	id=data.attr('data-id'),
	like=this.getAttribute('data-like'),
	emphasise=this.getAttribute('data-emphasise');
	// confirm("id= "+id+" like="+like);
	
	if(data.hasClass("mark")){
		return;
	}
	else if(emphasise=='emphasise'){
		var data1=data.closest('.vote').find('.float .mark');
		data1.removeClass('mark');
		var count1 = data1.next('span').text();
		count1--;
		data1.next('span').text(count1);

		data.addClass('mark');
		var count = data.next('span').text();
		count++;
		data.next('span').text(count);
		
	}
	else if(emphasise==''){
		data.closest('li').addClass('emphasise');
		data.addClass('mark');
		var count=data.next('span').text();
		count++;
		data.next('span').text(count);
		
		var data1=data.closest('.vote');
		data1.find('.float .like').attr('data-emphasise', 'emphasise');
		data1.find('.float .unlike').attr('data-emphasise', 'emphasise');
	}

	jQuery.ajax({
		url: my_object.ajax_url,
		type: 'POST',
		beforeSend: function(xml){
			// jQuery('.btmVote').css("cursor", 'wait' );
		},
		complete: function(xml){
			// jQuery('.btmVote').css("cursor", "pointer");
		},
		data: {
			action: 'my_action_ranker',
			id: id,
			like: like
		},
		success: function(msg){
			 console.log(msg);
			if(msg==-1)
				return;
			var p=jQuery.parseJSON(msg);
		
			jQuery('.like_'+id).text(p['like']);
			jQuery('.unlike_'+id).text(p['unlike']);
		}
	});
});