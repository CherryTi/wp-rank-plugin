<?php
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 29.07.2016
 * Time: 22:20
 */

namespace Uniranker\sys;


class Validator
{
    public static function isInt($var){
        return is_int($var);
    }

    public static function sanitizeText($text){
        return filter_var($text,FILTER_SANITIZE_STRING);
    }
}