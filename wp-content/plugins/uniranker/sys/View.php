<?php
namespace Uniranker\sys;
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 22.07.2016
 * Time: 9:19
 */
class View
{
    private $templatePath = UNIRANKER_VIEWS;

    public function render($template, $vars=array())
    {
        if (!file_exists($this->templatePath . $template)) {

        }
        extract($vars);
        ob_start();
        require $this->templatePath . $template.'.php';
        ob_get_flush();
    }

    public function prepareTemplate($template, $vars=array()){
        if (!file_exists($this->templatePath . $template)) {

        }
        extract($vars);
        ob_start();
        require $this->templatePath . $template.'.php';
        return ob_get_clean();
    }

    public function setTemplateFolder($templatePath)
    {
        $this->templatePath = $templatePath;
    }
}