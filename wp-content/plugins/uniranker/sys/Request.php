<?php
/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 24.07.2016
 * Time: 22:24
 */

namespace Uniranker\sys;


class Request
{
    public static function isPost(){
        return $_SERVER["REQUEST_METHOD"]=="POST";
    }

    public static function isGet(){
        return $_SERVER["REQUEST_METHOD"]=="GET";
    }

    public static function post($val=null){
        return (isset($_POST[$val]))?$_POST[$val]:$_POST;
    }

    public static function get($val){
        return (isset($_GET[$val]))?$_GET[$val]:false;
    }
}