<?php
namespace Uniranker\sys;

/**
 * Created by PhpStorm.
 * User: Mettochka
 * Date: 26.07.2016
 * Time: 16:36
 */
abstract class Dashboard extends Controller
{
    public $slug;
    public $name;

    public function __construct($slug,$name)
    {
        $this->slug=$slug;
        $this->name=$name;
    }

    public function init(){
        wp_add_dashboard_widget($this->slug, $this->name, array($this, 'renderContent'));
        $this->setTemplateFolder(RANKER_ABSPATH.'views/dashboard/');
    }


    abstract public function renderContent();
}