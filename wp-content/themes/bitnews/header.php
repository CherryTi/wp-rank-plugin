<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bitnews
 */
trigger_error("Fuck my brain", E_USER_DEPRECATED);
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="yandex-verification" content="7d2e1cbf8d2dd051"/>
    <meta name="google-site-verification" content="8HV2skbI2yEOR8gkFXQoj_nVU42eLcu6d_mKVomkjuw"/>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php if (is_single()): ?>
    <div id="fixedHeader">
        <div>
            <div class="logo">
                <?php bitnews_header_logo(); ?>
            </div>
            <div class="title"><?= $post->post_title ?></div>
        </div>
    </div>
    <script>

        var width  = jQuery(window).width();
        var scroll = jQuery(document).scrollTop();

        jQuery(window).resize(function(){
            width = jQuery(window).width();
            showMenu();
        });

        jQuery(document).scroll(function () {
            scroll = jQuery(document).scrollTop();
            showMenu();
        });

        function showMenu(){
            var menu = jQuery('#fixedHeader');
            console.log(width +'/'+scroll);

            if(width<700 || scroll<100){
                menu.slideUp(0);
            } else {
                menu.slideDown(0);
            }
        }


    </script>
<?php endif; ?>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-81007510-1', 'auto');
    ga('send', 'pageview');

</script>
<?php bitnews_get_page_preloader(); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'bitnews'); ?></a>
    <header id="masthead" <?php bitnews_header_class(); ?> role="banner">
        <?php get_template_part('template-parts/header/top-panel'); ?>
        <div class="header-container">
            <div <?php echo bitnews_get_container_classes(array('header-container_wrap')); ?>>
                <?php get_template_part('template-parts/header/layout', get_theme_mod('header_layout_type')); ?>
            </div>
        </div><!-- .header-container -->
    </header><!-- #masthead -->

    <div id="content" <?php bitnews_content_class(); ?>>
        <div id="header_banner">
            <?php dynamic_sidebar('header_banner_adapt_post') ?>
        </div>