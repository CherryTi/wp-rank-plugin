<?php get_header( bitnews_template_base() ); ?>

	<?php do_action( 'bitnews_render_widget_area', 'full-width-header-area' ); ?>

	<?php bitnews_site_breadcrumbs(); ?>

	<div class="container">

		<?php do_action( 'bitnews_render_widget_area', 'before-content-area' ); ?>

		<div class="row">

			<div id="primary" <?php bitnews_primary_content_class(); ?>>

				<?php do_action( 'bitnews_render_widget_area', 'before-loop-area' ); ?>

				<?php bitnews_home_title(); ?>

				<main id="main" class="site-main" role="main">

					<?php include bitnews_template_path(); ?>

				</main><!-- #main -->

				<?php do_action( 'bitnews_render_widget_area', 'after-loop-area' ); ?>

			</div><!-- #primary -->

			<?php get_sidebar( 'secondary' ); // Loads the sidebar-secondary.php template. ?>

			<?php get_sidebar( 'primary' ); // Loads the sidebar-primary.php template.  ?>

		</div><!-- .row -->

		<?php do_action( 'bitnews_render_widget_area', 'after-content-area' ); ?>

	</div><!-- .container -->

	<?php do_action( 'bitnews_render_widget_area', 'after-content-full-width-area' ); ?>

<?php get_footer( bitnews_template_base() ); ?>
