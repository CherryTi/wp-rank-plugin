<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bitnews
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<div class="col-xs-12 col-xl-2">
			<?php bitnews_share_buttons( 'single' ); ?>
		</div>
		<div class="col-xs-12 col-xl-10">
			<header class="entry-header">
				<?php
					$utility = bitnews_utility()->utility;

					$categories_visible = bitnews_is_meta_visible( 'single_post_categories', 'single' ) ? 'true' : 'false' ;
					$utility->meta_data->get_terms(array(
						'visible'	=> $categories_visible,
						'type'		=> 'category',
						'before'	=> '<div class="post__cats small-bold">',
						'after'		=> '</div>',
						'class'		=> '',
						'echo'		=> true,
					));

					$utility->attributes->get_title(array(
						'class'	=> 'entry-title h2-style',
						'html'	=> '<h1 %1$s>%4$s</h1>',
						'echo'	=> true,
					) );
				?>
				<?php if ( 'post' === get_post_type() ) : ?>

					<div class="entry-meta small-bold">

						<span class="post__date">
							<?php
								$date_visible = bitnews_is_meta_visible( 'single_post_publish_date', 'single' ) ? 'true' : 'false' ;
								$utility->meta_data->get_date(array(
									'visible'	=> $date_visible,
									'class'		=> 'post-date__link',
									'icon'		=> '',
									'echo'		=> true,
								));
						?>
						</span>
						<span class="post__author">
							<?php
								$author_visible = bitnews_is_meta_visible( 'single_post_author', 'single' ) ? 'true' : 'false' ;
								$utility->meta_data->get_author(array(
									'visible'	=> $author_visible,
									'prefix'	=> '',
									'echo'		=> true,
								));
							?>
						</span>
						<span class="post__comments">
							<?php
								$comment_visible = bitnews_is_meta_visible( 'single_post_comments', 'single' ) ? 'true' : 'false' ;
								$utility->meta_data->get_comment_count(array(
									'visible'	=> $comment_visible,
									'class'		=> 'post-date__link',
									'icon'		=> '',
									'echo'		=> true,
								));
							?>
						</span>

					</div><!-- .entry-meta -->

				<?php endif; ?>

			</header><!-- .entry-header -->

			<figure class="post-thumbnail">
				<?php
					$utility->media->get_image( array(
						'size'			=> 'bitnews-post-thumbnail-large',
						'class'			=> 'post-thumbnail__img wp-post-image',
						'html'			=> '<img %2$s class="post-thumbnail__img wp-post-image" src="%3$s" alt="%4$s" %5$s >',
						'placeholder'	=> false,
						'echo'			=> true,
					) );
				?>
				<div class="post-thumbnail__format-link">
					<?php do_action( 'cherry_post_format_link', array( 'render' => true, 'class' => 'invert' ) ); ?>
				</div>
			</figure><!-- .post-thumbnail -->

			<div class="entry-content">
				<?php $utility->attributes->get_content(array( 'echo' => true,) ); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bitnews' ),
						'after'  => '</div>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<?php
					$tags_visible = bitnews_is_meta_visible( 'single_post_tags', 'single' ) ? 'true' : 'false' ;

					$utility->meta_data->get_terms(array(
						'visible'	=> $tags_visible,
						'type'		=> 'post_tag',
						'class'		=> 'post__cats',
						'delimiter' => ' ',
						'icon'		=> '',
						'prefix'	=> '<span class="title-prefix">'.esc_html__( 'Tags: ', 'bitnews' ).'</span>',
						'before'	=> '<div class="post__tags small-bold">',
						'after'		=> '</div>',
						'echo'		=> true,
					));
				?>
			</footer><!-- .entry-footer -->
		</div>
	</div>
</article><!-- #post-## -->
