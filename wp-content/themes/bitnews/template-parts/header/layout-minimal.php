<?php
/**
 * Template part for minimal Header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bitnews
 */
?>
<div class="row row_vertical-center">
	<div class="col-xs-12 col-md-6 col-xl-3 social-col">
		<?php 
			bitnews_social_list( 'header' ); 
			bitnews_top_search( '<div class="top-panel__search on-desktop">%s</div>' );
		?>
	</div>

	<div class="col-xs-12 col-md-6 col-xl-3 branding-col">
		<div class="site-branding">
			<?php 
				bitnews_header_logo();
				bitnews_site_description(); 
			?>
		</div>
	</div>

	<div class="col-xs-12 mobile-block on-mobile">
		<?php 
			bitnews_top_search( '<div class="top-panel__search on-mobile">%s</div>' );
		?>
	</div>
	
	<div class="col-xs-12 col-xl-6 menu-col">
		<?php 
			bitnews_main_menu(); 
		?>
	</div>	
</div>