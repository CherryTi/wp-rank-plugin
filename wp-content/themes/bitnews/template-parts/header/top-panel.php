<?php
/**
 * Template part for top panel in header.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bitnews
 */

// Don't show top panel if all elements are disabled
if ( ! bitnews_is_top_panel_visible() ) {
	return;
} ?>

<div class="top-panel small">
	<div <?php echo bitnews_get_container_classes( array( 'top-panel__wrap' ) ); ?>>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<?php bitnews_top_message( '<div class="top-panel__message">%s</div>' ); ?>
			</div>
			<div class="col-xs-12 col-md-6">
				<?php bitnews_top_menu(); ?>
			</div>
		</div>
	</div>
</div><!-- .top-panel -->