<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package Bitnews
 */

?>

<div class="footer-container">
    <div <?php echo bitnews_get_container_classes(array('site-info')); ?>>
        <div class="row row_vertical-center">

            <div class="col-xs-12 footer_logo">
                <?php
                bitnews_footer_logo();
                ?>
            </div>
            <div class="col-xs-12 col-xl-5 logo-col">
                <?php
                $cats = get_categories();
                $cats = array_chunk($cats, ceil(count($cats)/2));
                if ($cats) {
                    foreach ($cats as $row):?>
                        <div class="footerCatRow">
                            <?php foreach ($row as $cat): ?>
                                <p><a href="/category/<?=$cat->slug?>"><?=$cat->name?></a></p>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach;
                } ?>
            </div>

            <div class="col-xs-12 col-xl-3 footer_middle">
                <div class="footer_soc">
                    <p><a target="_blank" href="https://www.facebook.com/groups/rerank/"><i
                                class="fa fa-facebook-f"></i><span>Подписаться на Facebook</span></a></p>
                    <p><a target="_blank" href="https://new.vk.com/rerank"><i class="fa fa-vk"></i><span>Подписаться на Вконтакте</span></a>
                    </p>
                </div>
            </div>

            <div class="col-xs-12 col-xl-4">
                <?php
                bitnews_footer_copyright();
                ?>
            </div>
        </div>
    </div><!-- .site-info -->
</div><!-- .container -->