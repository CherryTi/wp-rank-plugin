<?php
/**
 * The template for displaying the minimal footer layout.
 *
 * @package Bitnews
 */
?>

<div class="footer-container">
	<div <?php echo bitnews_get_container_classes( array( 'site-info' ) ); ?>>
		<div class="row">
			<div class="col-xs-12 copyright-menu-col small">
				<?php 
					bitnews_footer_copyright();
					bitnews_footer_menu();
				?>
			</div>			
		</div>
	</div><!-- .site-info -->
</div><!-- .container -->