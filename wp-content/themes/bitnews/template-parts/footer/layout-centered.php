<?php
/**
 * The template for displaying the default footer layout with widgets.
 *
 * @package Bitnews
 */

?>

<div class="footer-area-wrap invert">
	<div class="container">
		<?php do_action( 'bitnews_render_widget_area', 'footer-area' ); ?>
	</div>
</div>

<div class="footer-container">
	<div <?php echo bitnews_get_container_classes( array( 'site-info' ) ); ?>>
		<div class="row row_vertical-center">
			<div class="col-xs-12 col-xl-3 logo-col">
				<?php 
					bitnews_footer_logo();
				?>
			</div>

			<div class="col-xs-12 col-xl-6 copyright-menu-col small">
				<?php 
					bitnews_footer_copyright();
					bitnews_footer_menu();
				?>
			</div>
			
			<div class="col-xs-12 col-xl-3 social-col">
				<?php bitnews_social_list( 'footer' ); ?>
			</div>
		</div>
	</div><!-- .site-info -->
</div><!-- .container -->