<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bitnews
 */

?>

<?php 
	global $wp_query;

	$utility = bitnews_utility()->utility;
	$categories_visible = bitnews_is_meta_visible( 'blog_post_categories', 'loop' ) ? 'true' : 'false' ;
	$author_visible = bitnews_is_meta_visible( 'blog_post_author', 'loop' ) ? 'true' : 'false' ;

	$blog_layout = get_theme_mod( 'blog_layout_type', bitnews_theme()->customizer->get_default( 'blog_layout_type' ) );

	if ( 'vertical-justify' == $blog_layout ) {
		if ( ( $wp_query->current_post + 1 ) % 6 === 3 || ( $wp_query->current_post + 1 ) % 6 === 4	) {
			$categories_visible = true;
			$author_visible = true;	
		}
	}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'posts-list__item card' ); ?>>
	<div class="post-list__item-content">
		<div class="post-featured-content invert">
			<?php bitnews_sticky_label(); ?>
		</div><!-- .post-featured-content -->

		<div class="post-content_wrap">
			<?php
				$utility->meta_data->get_terms(array(
					'visible'	=> $categories_visible,
					'type'		=> 'category',
					'icon'		=> '',
					'before'	=> '<div class="post__cats small-bold">',
					'after'		=> '</div>',
					'class'		=> 'post__cats',
					'echo'		=> true,
				));
			?>
			<header class="entry-header">
				<?php
					$title_html = ( is_single() ) ? '<h1 %1$s>%4$s</h1>' : '<h4 %1$s><a href="%2$s" %3$s rel="bookmark">%4$s</a></h4>' ;

					$utility->attributes->get_title(array(
						'class'	=> 'entry-title',
						'html'	=> $title_html,
						'echo'	=> true,
					) );
				?>
			</header><!-- .entry-header -->

			<?php if ( 'post' === get_post_type() ) : ?>

				<div class="entry-meta small-bold">
					<?php
						$date_visible = bitnews_is_meta_visible( 'blog_post_publish_date', 'loop' ) ? 'true' : 'false' ;

						$utility->meta_data->get_date(array(
							'visible'	=> $date_visible,
							'class'		=> 'post-date__link',
							'icon'		=> '',
							'echo'		=> true,
							'html'		=> '<span class="post__date">%1$s<a href="%2$s" %3$s %4$s ><time datetime="%5$s">%6$s%7$s</time></a></span>',
						));
					?>
					<?php
						$utility->meta_data->get_author(array(
							'visible'	=> $author_visible,
							'prefix'	=> '',
							'echo'		=> true,
							'html'		=> '<span class="post__author">%1$s<a href="%2$s" %3$s %4$s rel="author">%5$s%6$s</a></span>',
						));
					?>
					<?php
						$comment_visible = bitnews_is_meta_visible( 'blog_post_comments', 'loop' ) ? 'true' : 'false' ;

						$utility->meta_data->get_comment_count(array(
							'visible'	=> $comment_visible,
							'class'		=> 'post-date__link',
							'icon'		=> '<i class="material-icons">chat</i>',
							'echo'		=> true,
							'html'		=> '<span class="post__comments">%1$s<a href="%2$s" %3$s %4$s>%5$s%6$s</a></span>',
						));
					?>
				
					<?php
						$tags_visible = bitnews_is_meta_visible( 'blog_post_tags', 'loop' ) ? 'true' : 'false' ;

						$utility->meta_data->get_terms(array(
							'visible'	=> $tags_visible,
							'type'		=> 'post_tag',
							'class'		=> 'post__cats',
							'delimiter' => ', ',
							'icon'		=> '',
							'before'	=> '<div class="post__tags">',
							'after'		=> '</div>',
							'echo'		=> true,
						));
					?>
				</div><!-- .entry-meta -->

			<?php endif; ?>

			<div class="post-featured-content invert">
				<?php do_action( 'cherry_post_format_audio' ); ?>
			</div><!-- .post-featured-content -->

			<div class="content-footer_group">

				<div class="entry-content">
					<?php
						$embed_args = array(
							'fields' => array( 'soundcloud' ),
							'height' => 310,
							'width'  => 310,
						);
						$embed_content = apply_filters( 'cherry_get_embed_post_formats', false, $embed_args );

						if ( false === $embed_content ) {
							$blog_content = get_theme_mod( 'blog_posts_content', bitnews_theme()->customizer->get_default( 'blog_posts_content' ) );
							
							if ( 'full' === $blog_content ) {
								$length = 0;
								$echo = true;
							} elseif ( 'excerpt' === $blog_content ) {
								$length = 30;
								$echo = true;
							} else {
								$length = 0;
								$echo = false;
							}

							if ( 'vertical-justify' == $blog_layout ) {
								if ( ( $wp_query->current_post + 1 ) % 6 === 3 || ( $wp_query->current_post + 1 ) % 6 === 4	) {
									$length = ( 'full' === $blog_content ) ? 0 : 15;
								}
							}
												
							$utility->attributes->get_content(array(
								'length'		=> $length,
								'content_type'	=> 'post_excerpt',
								'echo'			=> $echo,
							) );
						} else {
							printf( '<div class="embed-wrapper">%s</div>', $embed_content );
						}
					?>
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php bitnews_share_buttons( 'loop' ); ?>
					<?php
						$show_button = get_theme_mod( 'blog_post_show_button', bitnews_theme()->customizer->get_default( 'blog_post_show_button' ) );

						$utility->attributes->get_button(array(
							'text'		=> get_theme_mod( 'blog_read_more_text', bitnews_theme()->customizer->get_default( 'blog_read_more_text' ) ),
							'icon'		=> '',
							'echo'		=> $show_button,
						) );
					?>
				</footer><!-- .entry-footer -->

			</div>

		</div>
	</div>
</article><!-- #post-## -->
