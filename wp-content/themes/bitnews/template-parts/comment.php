<footer class="comment-meta">
	<div class="comment-author vcard">
		<?php echo bitnews_comment_author_avatar(); ?>
	</div>
	<div class="comment-metadata small-bold">
		<?php echo bitnews_get_comment_date( array( 'format' => 'M d, Y' ) ); ?>
		<?php echo bitnews_get_comment_author_link(); ?>
		
	</div>
</footer>
<div class="comment-content">
	<?php echo bitnews_get_comment_text(); ?>
</div>
<div class="reply">
	<?php echo bitnews_get_comment_reply_link( array( 'reply_text' => '<i class="material-icons">reply</i>' ) ); ?>
</div>