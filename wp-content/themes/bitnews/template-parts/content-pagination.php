<?php
/**
 * Template part for posts pagination.
 *
 * @package Bitnews
 */
the_posts_pagination(
	array(
		'prev_text' => '<i class="material-icons">navigate_before</i> ' . __('НАЗАД', 'bitnews'),
		'next_text' => __('ВПЕРЕД', 'bitnews') . ' <i class="material-icons">navigate_next</i>'
	)
);
