<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bitnews
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			$utility = bitnews_utility()->utility;

			$utility->attributes->get_title(array(
				'class'	=> 'entry-title h5-style',
				'html'	=> '<h1 %1$s>%4$s</h1>',
				'echo'	=> true,
			) );
		 ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php $utility->attributes->get_content(array( 'echo' => true,) ); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bitnews' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'EDIT %s', 'bitnews' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
