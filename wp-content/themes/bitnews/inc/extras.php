<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Bitnews
 */

/**
 * Set post specific sidebar position
 *
 * @param  string $position Default sidebar position.
 * @return string
 */
function bitnews_set_sidebar_position( $position ) {
	$queried_obj = apply_filters( 'bitnews_queried_object_id', false );

	if ( ! $queried_obj ) {

		if ( ! is_singular() ) {
			return $position;
		}

		if ( is_front_page() && 'page' !== get_option( 'show_on_front' ) ) {
			return $position;
		}

	}

	$queried_obj = ( ! $queried_obj ) ? get_the_id() : $queried_obj;

	if ( ! $queried_obj ) {
		return $position;
	}

	$post_position = get_post_meta( $queried_obj, 'bitnews_sidebar_position', true );

	if ( ! $post_position || 'inherit' === $post_position ) {
		return $position;
	}

	return $post_position;

}
add_filter( 'theme_mod_sidebar_position', 'bitnews_set_sidebar_position' );

/**
 * Render existing macros in passed string.
 *
 * @since  1.0.0
 * @param  string $string String to parse.
 * @return string
 */
function bitnews_render_macros( $string ) {

	$macros = apply_filters( 'bitnews_data_macros', array(
		'/%%year%%/' => date( 'Y' ),
		'/%%date%%/' => date( get_option( 'date_format' ) ),
	) );

	return preg_replace( array_keys( $macros ), array_values( $macros ), $string );

}

/**
 * Render font icons in content
 *
 * @param  string $content content to render
 * @return string
 */
function bitnews_render_icons( $content ) {
	$icons     = bitnews_get_render_icons_set();
	$icons_set = implode( '|', array_keys( $icons ) );

	$regex = '/icon:(' . $icons_set . ')?:?([a-zA-Z0-9-_]+)/';

	return preg_replace_callback( $regex, 'bitnews_render_icons_callback', $content );
}

/**
 * Callback for icons render.
 *
 * @param  array $matches Search matches array.
 * @return string
 */
function bitnews_render_icons_callback( $matches ) {

	if ( empty( $matches[1] ) && empty( $matches[2] ) ) {
		return $matches[0];
	}

	if ( empty( $matches[1] ) ) {
		return sprintf( '<i class="fa fa-%s"></i>', $matches[2] );
	}

	$icons = bitnews_get_render_icons_set();

	if ( ! isset( $icons[ $matches[1] ] ) ) {
		return $matches[0];
	}

	return sprintf( $icons[ $matches[1] ], $matches[2] );
}

/**
 * Get list of icons to render.
 *
 * @return array
 */
function bitnews_get_render_icons_set() {
	return apply_filters( 'bitnews_render_icons_set', array(
		'fa'       => '<i class="fa fa-%s"></i>',
		'material' => '<i class="material-icons">%s</i>',
	) );
}

/**
 * Replace %s with theme URL.
 *
 * @param  string $url Formatted URL to parse.
 * @return string
 */
function bitnews_render_theme_url( $url ) {
	return sprintf( $url, get_stylesheet_directory_uri() );
}

/**
 * Get image ID by URL.
 *
 * @param  string $image_src Image URL to search it in database.
 * @return int|bool false
 */
function bitnews_get_image_id_by_url( $image_src ) {
	global $wpdb;

	$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
	$id    = $wpdb->get_var( $query );

	return $id;
}

function bitnews_post_formats_gallery() {
	$size = bitnews_post_thumbnail_size();

	if ( ! in_array( get_theme_mod( 'blog_layout_type' ), array( 'masonry-2-cols', 'masonry-3-cols' ) ) ) {
		return do_action( 'cherry_post_format_gallery', array(
			'size' => $size[ 'size' ],
		) );
	}

	$images = bitnews_theme()->get_core()->modules['cherry-post-formats-api']->get_gallery_images(false);

	if ( is_string( $images ) && ! empty( $images ) ) {
		return $images;
	}

	$items             = array();
	$first_item        = null;
	$size              = $size[ 'size' ];
	$format            = '<div class="mini-gallery post-thumbnail--fullwidth">%1$s<div class="post-gallery__slides" style="display: none;">%2$s</div></div>';
	$first_item_format = '<a href="%1$s" class="post-thumbnail__link">%2$s</a>';
	$item_format       = '<a href="%1$s">%2$s</a>';

	foreach( $images as $img ) {
		$image = wp_get_attachment_image( $img, $size );
		$url   = wp_get_attachment_url( $img );

		if ( sizeof( $items ) === 0 ) {
			$first_item = sprintf( $first_item_format, $url, $image );
		}

		$items[] = sprintf( $item_format, $url, $image );
	}

	printf( $format, $first_item, join( "\r\n", $items ) );
}

/**
 * Check if passed meta data is visible in current context.
 *
 * @since  1.0.0
 * @param  string $meta    Meta setting to check.
 * @param  string $context Current post context - 'single' or 'loop'.
 * @return bool
 */
function bitnews_is_meta_visible( $meta, $context = 'loop' ) {

	if ( ! $meta ) {
		return false;
	}

	$meta_enabled = get_theme_mod( $meta, bitnews_theme()->customizer->get_default( $meta ) );

	switch ( $context ) {

		case 'loop':

			if ( ! is_single() && $meta_enabled ) {
				return true;
			} else {
				return false;
			}

		case 'single':

			if ( is_single() && $meta_enabled ) {
				return true;
			} else {
				return false;
			}

	}

	return false;
}

/**
 * Get post thumbnail size.
 *
 * @return array
 */
function bitnews_post_thumbnail_size( $args = array() ) {
	global $wp_query;

	$args = wp_parse_args( $args, array(
		'small'        => 'post-thumbnail',
		'fullwidth'    => 'bitnews-post-thumbnail-large',
		'justify_small'=> 'bitnews-thumb-352x253',
		'justify_big'  => 'bitnews-thumb-754x350',
		'class_prefix' => '',
	) );

	$layout      = get_theme_mod( 'blog_layout_type', bitnews_theme()->customizer->get_default( 'blog_layout_type' ) );
	$format      = get_post_format();
	$size_option = get_theme_mod( 'blog_featured_image', bitnews_theme()->customizer->get_default( 'blog_featured_image' ) );
	$size        = $args[ $size_option ];
	$link_class  = sanitize_html_class( $args['class_prefix'] . $size_option );

	if ( 'default' !== $layout
		|| is_single()
		|| in_array( $format , array( 'image', 'gallery' ) )
	) {
		if ( 'vertical-justify' == $layout ) {
			if ( ( $wp_query->current_post + 1 ) % 6 === 3 || ( $wp_query->current_post + 1 ) % 6 === 4	) {
				$size       = $args['justify_big'];
				$link_class = $args['class_prefix'] . 'justify_big';
			} else {
				$size       = $args['justify_small'];
				$link_class = $args['class_prefix'] . 'justify_small';
			}
		} else if ( 'masonry-2-cols' == $layout ) {
			if ( ($wp_query->current_post + 1) % 2 === 0 ) {
				$size = 'bitnews-thumb-486x350';
				$link_class = $args['class_prefix'] . 'bitnews-thumb-486x350';
			} else {
				$size = 'bitnews-thumb-486x589';
				$link_class = $args['class_prefix'] . 'bitnews-thumb-486x589';
			}
		} else if ( 'masonry-3-cols' == $layout ) {
			if ( ($wp_query->current_post + 1) % 3 === 0 ) {
				$size = 'bitnews-thumb-486x350';
				$link_class = $args['class_prefix'] . 'bitnews-thumb-486x350';
			} else if ( ($wp_query->current_post + 1) % 3 === 1 ) {
				$size = 'bitnews-thumb-486x589';
				$link_class = $args['class_prefix'] . 'bitnews-thumb-486x589';
			}
		} else if ( 'default' == $layout ) {
			$size 		= 'bitnews-thumb-l';
			$link_class = $args['class_prefix'] . 'fullwidth';
		} else {
			$size       = $args['fullwidth'];
			$link_class = $args['class_prefix'] . 'fullwidth';
		}
	}

	if ( 'default' == $layout && 'small' == $size_option) {
		$size = 'bitnews-thumb-486x350';
		$link_class = $args['class_prefix'] . 'small';
	}

	return array(
		'size'  => $size,
		'class' => $link_class,
	);
}

/**
 * Get blog title.
 *
 * @return array
 */
function bitnews_home_title() {

	$blog_title = get_theme_mod( 'blog_title', bitnews_theme()->customizer->get_default( 'blog_title' ) );

	if ( $blog_title == '' ) {
		return;
	}
	
	if ( is_home() && is_front_page() ) {
		$format = '<div class="blog-title"><h5>%s</h5></div>';

		printf( $format, $blog_title );
	}
}