<?php
/**
 * Template part to display Instagram follow list widget.
 *
 * @package Editorso
 * @subpackage widgets
 */
?>

<div class="instagram__item">
	<?php echo $image; ?>
	<div class="small-bold"><?php echo $date; ?></div>
	<div class="instagram__caption small-bold"><?php echo $caption; ?></div>
</div>