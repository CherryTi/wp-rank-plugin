<div class="post <?php echo $grid_class; ?>">
	<div class="post-inner">
		<div class="post-image">
			<?php echo $image; ?>
		</div>
		<div class="post-content">
			<?php echo $title; ?>

			<div class="post-meta small-bold">
				<?php echo $date; ?>
				<?php echo $author; ?>
				<?php echo $count; ?>
				<?php echo $category; ?>
				<?php echo $tag; ?>
			</div>

			<?php echo $excerpt; ?>			
			<?php echo $button; ?>
		</div>
		<div class="clear"></div>

	</div>
</div>