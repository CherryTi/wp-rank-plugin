<?php
/**
 * Template part to display Carousel widget.
 *
 * @package Bitnews
 * @subpackage widgets
 */
?>

<div class="inner">
	<div class="content-wrapper">
		<header class="entry-header">
			<?php echo $image; ?>
		</header>
		<div class="entry-content">
			<?php echo $terms_line; ?>

			<?php echo $title; ?>

			<div class="entry-meta">
				<?php echo $date; ?>
				<?php echo $author; ?>
			</div>

			<?php echo $content; ?>
			<?php echo $more_button; ?>
		</div>
	</div>
</div>