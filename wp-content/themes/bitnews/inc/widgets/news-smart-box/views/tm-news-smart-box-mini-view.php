<div class="inner">
	<header class="entry-header">
		<?php echo $image; ?>
	</header>
	<div class="entry-content">
		<h6><?php echo $title; ?></h6>
		<?php 
			if ( 'true' == $this->instance['content_visibility'] ) {
				echo $content; 
			} 
		?>
		<div class="entry-meta small-bold">
			<?php if ( 'true' == $this->instance['date_visibility'] ) { ?>
				<span class="post__date"><?php echo $date; ?></span> <?php
			} ?>
		</div>
	</div>
	<div class="clear"></div>
</div>