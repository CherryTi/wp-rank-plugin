<div class="inner">
	<header class="entry-header">
		<?php echo $image_full; ?>
	</header>
	<div class="entry-content">
		<div class="post__cats small-bold"><?php echo $terms_line; ?></div>
		<h2><?php echo $title; ?></h2>
		<div class="entry-meta small-bold">
			<?php if ( 'true' == $this->instance['date_visibility'] ) { ?>
				<span class="post__date"><?php echo $date; ?></span> <?php
			} ?>
			<?php if ( 'true' == $this->instance['author_visibility'] ) { ?>
				<span class="post__author vcard"><?php echo $author; ?></span> <?php
			} ?>
			<?php if ( 'true' == $this->instance['comment_visibility'] ) { ?>
				<span class="post__comments"><i class="material-icons">chat</i><?php echo $comments; ?></span> <?php
			} ?>
		</div>
		<?php 
			if ( 'true' == $this->instance['content_visibility'] ) {
				echo $content; 
			} 
		?>
	</div>
</div>