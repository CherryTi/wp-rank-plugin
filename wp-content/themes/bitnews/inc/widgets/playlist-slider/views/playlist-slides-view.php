<div class="sp-slide post-format-<?php echo $post_format; ?>">
	<div class="sp-slide-content">
		<?php echo $category; ?>
		<?php echo $title; ?>
		<?php echo $author; ?>
		<?php echo $date; ?>
		<?php echo $tag; ?>
	</div>
	<div class="sp-slide-preview">
		<?php echo $slide; ?>
	</div>
</div>