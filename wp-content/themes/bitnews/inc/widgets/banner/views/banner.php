<?php
/**
 * Template part to display Banner widget.
 *
 * @package __Tm
 * @subpackage widgets
 */
?>

<a class="widget-banner__link" href="<?php echo esc_url( $link ); ?>" target="<?php echo esc_attr( $target ); ?>">
	<img class="widget-banner__img" src="<?php echo esc_url( $src[0] ); ?>" width="<?php echo esc_attr( $src[1] ); ?>" height="<?php echo esc_attr( $src[2] ); ?>" alt="<?php echo esc_attr( $title ); ?>">
	<div class="widget-banner__descr invert"><div class="widget-banner__title h2-style"><span><?php echo esc_attr($title). '</span></div><div class="widget-banner__text h6-style"><span>' .esc_attr($descr); ?></span></div></div>
</a>