<?php
/**
 * Template part to display subscribe form.
 *
 * @package Bitnews
 * @subpackage widgets
 */
?>
<?php 
	$media_id = absint( $instance['media_id'] );
	$src      = wp_get_attachment_image_src( $media_id, 'bitnews-thumb-1158x331' );

	echo '<div class="widget-subscribe_with_bg"><div class="subscribe-bg-image" style="background-image:url('.$src[0].')"></div><div class="subscribe-bg-color"></div>';
?>