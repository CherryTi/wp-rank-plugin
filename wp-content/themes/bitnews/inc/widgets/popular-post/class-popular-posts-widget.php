<?php
/**
 * Widget custom posts.
 *
 * @package Bitnews
 */

if ( ! class_exists( 'Bitnews_Popular_Posts_Widget' ) ) {

	class Bitnews_Popular_Posts_Widget extends Cherry_Abstract_Widget {

		/**
		 * Contain utility module from Cherry framework
		 *
		 * @since 1.0.0
		 * @var   object
		 */
		private $utility = null;

		/**
		 * Constructor
		 *
		 * @since  1.0.0
		 */
		public function __construct() {
			$this->widget_name			= esc_html__( 'Popular Posts', 'bitnews' );
			$this->widget_description 	= esc_html__( 'Display popular posts your site by number of comments.', 'bitnews' );
			$this->widget_id			= apply_filters( 'bitnews_custom_posts_widget_ID', 'widget-popular-postson' );
			$this->widget_cssclass		= apply_filters( 'bitnews_custom_posts_widget_cssclass', 'widget-popular-postson' );
			$this->utility				= bitnews_utility()->utility;
			$this->settings 			= array(
				'title' => array(
					'type'				=> 'text',
					'value'				=> esc_html__( 'Popular Posts', 'bitnews' ),
					'label'				=> esc_html__( 'Title', 'bitnews' ),
				),
				
				'posts_per_page' => array(
					'type'				=> 'stepper',
					'value'				=> 3,
					'max_value'			=> 50,
					'min_value'			=> 0,
					'label'				=> esc_html__( 'Posts count ( Set 0 to show all. )', 'bitnews' ),
				),
				'title_length' => array(
					'type'				=> 'stepper',
					'value'				=> '10',
					'max_value'			=> '30',
					'min_value'			=> '0',
					'step_value'		=> '1',
					'label'				=> esc_html__( 'Title words length ( Set 0 to hide title. )', 'bitnews' ),
				),
				'excerpt_length' => array(
					'type'				=> 'stepper',
					'value'				=> '10',
					'max_value'			=> '30',
					'min_value'			=> '0',
					'step_value'		=> '1',
					'label'				=> esc_html__( 'Excerpt words length ( Set 0 to hide excerpt. )', 'bitnews' ),
				),
				'meta_data' => array(
					'type'				=> 'checkbox',
					'value'				=> array(
						'date'				=> 'true',
						'author'			=> 'false',
						'comment_count'		=> 'false',
						'category'			=> 'false',
						'tag'				=> 'false',
						'more_button'				=> 'false',
					),
					'options'				=> array(
						'date'				=> esc_html__( 'Date', 'bitnews' ),
						'author'			=> esc_html__( 'Author (always displays on every 1st and 6th posts)', 'bitnews' ),
						'comment_count'		=> esc_html__( 'Comment count', 'bitnews' ),
						'category'			=> esc_html__( 'Category (always displays on every 1st and 6th posts)', 'bitnews' ),
						'post_tag'			=> esc_html__( 'Tag', 'bitnews' ),
						'more_button'		=> esc_html__( 'More Button', 'bitnews' ),
					),
					'label'				=> esc_html__( 'Display post meta data', 'bitnews' ),
				),
				'button_text' => array(
					'type'				=> 'text',
					'value'				=> 'Read More',
					'label'				=> esc_html__( 'Post read more button label', 'bitnews' ),
				),
			);

			parent::__construct();
		}

		/**
		 * widget function.
		 *
		 * @see WP_Widget
		 *
		 * @since  1.0.0
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {
			if ( $this->get_cached_widget( $args ) ) {
				return;
			}

			ob_start();

			$this->setup_widget_data( $args, $instance );
			$this->widget_start( $args, $instance );

			extract( $instance, EXTR_OVERWRITE );

			$posts_per_page  = ( '0' === $posts_per_page ) ? -1 : ( int ) $posts_per_page ;
			$post_args = array(
				'post_type'		=> 'post',
				'numberposts'	=> $posts_per_page,
				'orderby' 		=> 'comment_count',
			);

			$posts = get_posts( $post_args );

			if ( $posts ) {
				global $post;

				$i = 1;
				
				$holder_view_dir = locate_template( 'inc/widgets/popular-post/views/popular-post-view.php' );

				echo '<div class="popular-posts-holder row" >';

					if ( $holder_view_dir ) {
						foreach ( $posts as $post ) {
							setup_postdata( $post );
							
							$custom_class = '';
							$size = 'bitnews-thumb-352x253';

//							if ( $args['id'] !== 'sidebar-primary' && $args['id'] !== 'sidebar-secondary' && $args['id'] !== 'footer-area' ) {
//								if ( $i % 6 === 1 || $i % 6 === 0 ) {
//									$custom_class = 'bigger';
//									$size = 'bitnews-thumb-754x350';
//								}
//							}

							$image = $this->utility->media->get_image( array(
								'size'			=> $size,
							) );

							$excerpt_visible = ( '0' === $excerpt_length ) ? false : true ;
							$excerpt = $this->utility->attributes->get_content( array(
								'visible'		=> $excerpt_visible,
								'length'		=> $excerpt_length,
								'content_type'	=> 'post_excerpt',
							) );

							$title_visible = ( '0' === $title_length ) ? false : true ;
							$title = $this->utility->attributes->get_title( array(
								'visible'		=> $title_visible,
								'length'		=> $title_length,
								'html'			=> '<h4 %1$s><a href="%2$s" %3$s>%4$s</a></h4>',
							) );

							$permalink = $this->utility->attributes->get_post_permalink();

							$date = $this->utility->meta_data->get_date( array(
								'visible'		=> $meta_data['date'],
							) );

							$count = $this->utility->meta_data->get_comment_count( array(
								'visible'		=> $meta_data['comment_count'],
								'sufix'			=> _n_noop( '%s comment', '%s comments', 'bitnews' ),
							) );

							if ( $i % 6 === 1 || $i % 6 === 0 ) {
								$visible_category = 'true';
								$visible_author = 'true';
							} else {
								$visible_category = $meta_data['category'];
								$visible_author = $meta_data['author'];
							}

//							$author = $this->utility->meta_data->get_author( array(
//								'visible'		=> $visible_author,
//							) );

							$category = $this->utility->meta_data->get_terms( array(
								'delimiter'		=> '',
								'type'			=> 'category',
								'visible'		=> $visible_category,
								'before' 		=> '<div class="post__cats small-bold">',
								'after'			=> '</div>',
							) );

							$tag = $this->utility->meta_data->get_terms( array(
								'delimiter'		=> ', ',
								'type'			=> 'post_tag',
								'visible'		=> $meta_data['post_tag'],
							) );

							$button = $this->utility->attributes->get_button( array(
								'visible'		=> $meta_data['more_button'],
								'text'			=> $button_text,
								'icon'			=> '',
							) );

							require( $holder_view_dir );

							$i++;
						}
					}

				echo '</div>';
			}

			$this->widget_end( $args );
			$this->reset_widget_data();
			wp_reset_postdata();

			echo $this->cache_widget( $args, ob_get_clean() );
		}
	}

	add_action( 'widgets_init', 'bitnews_register_popular_posts_widget' );
	function bitnews_register_popular_posts_widget() {
		register_widget( 'Bitnews_Popular_Posts_Widget' );
	}
}