<?php
/**
 * Menus configuration.
 *
 * @package Bitnews
 */

add_action( 'after_setup_theme', 'bitnews_register_menus', 5 );
function bitnews_register_menus() {

	// This theme uses wp_nav_menu() in four locations.
	register_nav_menus( array(
		'top'    => esc_html__( 'Top', 'bitnews' ),
		'main'   => esc_html__( 'Main', 'bitnews' ),
		'footer' => esc_html__( 'Footer', 'bitnews' ),
		'social' => esc_html__( 'Social', 'bitnews' ),
	) );
}
