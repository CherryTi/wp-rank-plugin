<?php
/**
 * Thumbnails configuration.
 *
 * @package Bitnews
 */

add_action( 'after_setup_theme', 'bitnews_register_image_sizes', 5 );
function bitnews_register_image_sizes() {
	set_post_thumbnail_size( 352, 253, true );

	// Registers a new image sizes.
	add_image_size( 'bitnews-thumb-xs', 129, 93, true );
	add_image_size( 'bitnews-thumb-s', 205, 80, true );
	add_image_size( 'bitnews-thumb-m', 300, 216, true );
	add_image_size( 'bitnews-thumb-352x253', 352, 253, true );
	add_image_size( 'bitnews-thumb-486x350', 486, 350, true );
	add_image_size( 'bitnews-thumb-486x543', 486, 543, true );
	add_image_size( 'bitnews-thumb-486x589', 486, 589, true );
	add_image_size( 'bitnews-thumb-486x640', 486, 640, true );
	add_image_size( 'bitnews-thumb-620x350', 620, 350, true );
	add_image_size( 'bitnews-thumb-754x350', 754, 350, true );
	add_image_size( 'bitnews-thumb-888x750', 888, 750, true );	
	add_image_size( 'bitnews-post-thumbnail-large', 888, 544, true );
	add_image_size( 'bitnews-thumb-l', 1022, 625, true );
	add_image_size( 'bitnews-thumb-1158x331', 1158, 331, true );
	add_image_size( 'bitnews-thumb-xl', 1920, 750, true );

	add_image_size( 'medium', get_option( 'medium_size_w' ), get_option( 'medium_size_h' ), true );
	add_image_size( 'large', get_option( 'large_size_w' ), get_option( 'large_size_h' ), true );
}