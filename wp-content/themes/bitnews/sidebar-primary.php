<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bitnews
 */
$sidebar_position 			= get_theme_mod( 'sidebar_position' );
$sidebars_on_home_page 		= true;

if ( is_home() || is_front_page() || is_category() ) {
	$sidebars_on_home_page = get_theme_mod( 'sidebars_on_home_page', bitnews_theme()->customizer->get_default( 'sidebars_on_home_page' ) );
}

if ( is_404() ) {
	return;
}

if ( 'fullwidth' === $sidebar_position || !$sidebars_on_home_page ) {
	return;
}

if ( ! is_active_sidebar( 'sidebar-primary' ) ) {
	return;
} ?>

<?php do_action( 'bitnews_render_widget_area', 'sidebar-primary' ); ?>
