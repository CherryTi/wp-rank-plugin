<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bitnews
 */
$categories = get_categories(array(
    'exclude' => '149',
));
$catColors = array(
    '0' => '#1FB6FF',
    '1' => '#7E5BEF',
    '2' => '#FF49DB',
    '3' => '#FF7849',
    '4' => '#12CE66',
    '5' => '#FFC82C',
    '6' => '#009EEB',
    '7' => '#FF5216',
    '8' => '#0F9F4F'
);

$catIcons = array(
    'Бренды' => 'fa-trademark',
    'Игры' => 'fa-gamepad',
    'Люди' => 'fa-user',
    'Места' => 'fa-plane',
    'Музыка' => 'fa-music',
    'Спорт' => 'fa-futbol-o',
    'Фильмы' => 'fa-film',
);


if (have_posts()) :

    if (is_home() && !is_front_page()) : ?>
        <header>
            <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
        </header>

    <?php endif; ?>

    <div <?php bitnews_posts_list_class(); ?>>
        <div class="events">
            <?php
            $options = array();
            $options = array(
                'category' => 149,
                'posts_per_page' => 4,
                'offset' => 0
            );
            $events = get_posts($options);
            if ($events):


                echo "<div class='eventRow'>";
                foreach ($events as $event):?>
                    <div class="event"
                         style="background: url('<?= get_the_post_thumbnail_url($event) ?>');background-size: cover">
                        <div class="descr">
                            <span class="title"><?=$event->post_title?></span>
                        </div>
                        <div class="eventDate">
                            <div class="eventDay">23</div>
                            <div class="eventMonth">Авг.</div>
                        </div>
                    </div>
                    <?php
                endforeach;
                echo "</div>";

            endif;
            ?>
        </div>
        <?php /* Start the Loop */
        $i = 0;
        foreach ($categories as $cat) {
            if ($i == count($catColors)) $i = 0;
            $color = $catColors[$i++];
            $category = mb_strtolower($cat->name);
            echo "<div class='catname'><a href=\"/category/$category\"><i style='background-color: $color' class=\"icon fa {$catIcons[$cat->name]}\"></i><h5 style=\"color: $color\">$cat->name</a></h5></div>";
            echo "<div class='categoryRow'>";
            $options = array();
            $options = array(
                'posts_per_page' => 4,
                'offset' => 0,
                'category' => $cat->term_id,
                'orderby' => 'date',
                'order' => 'DESC',
                'post_type' => 'post',
                'post_status' => 'publish',
            );
            $posts = get_posts($options);

            $posts = array_chunk($posts, 4);

            foreach ($posts as $row) {
                echo "<div class='singleCatRow'>";
                foreach ($row as $post) {
                    get_template_part('template-parts/content', get_post_format());
                }
                echo "</div>";

            }
            echo "</div>";
            echo "<div class='clear'></div>";
        }
        ?>

    </div><!-- .posts-list -->

    <?php

else :

    get_template_part('template-parts/content', 'none');

endif; ?>
