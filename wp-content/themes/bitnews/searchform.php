<?php
/**
 * The template for displaying search form.
 *
 * @package Bitnews
 */
?>
<div class="search-form_wrap">
	<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<label>
			<span class="screen-reader-text"><?php echo _x( 'Что ищем?', 'label', 'bitnews' ) ?></span>
			<input type="search" class="search-form__field"
				id="serchFocus"
				placeholder="<?php echo esc_attr_x( 'Что ищем?', 'placeholder', 'bitnews' ) ?>"
				value="<?php echo get_search_query() ?>" name="s"
				title="<?php echo esc_attr_x( 'Что ищем?', 'label', 'bitnews' ) ?>" />
		</label>
		<button type="submit" class="search-form__submit btn"><?php echo esc_attr_x( 'ПОИСК', 'bitnews' ) ?></button>
	</form>

	<span class="close-btn"><a href="#" class="close-btn-link"><i class="material-icons">close</i></a></span>
	<span class="open-btn"><a href="#" onclick="serchFocus()" class="open-btn-link"><i class="material-icons">search</i></a></span>
</div>